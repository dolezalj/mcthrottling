###

bandwidthTests          .. Tests for measuring memory bandwidth of different
                           resources (GPU, A57 CPUs, Denver2 CPUs) written in C.
                          *Made upon deploy on TX2, cuda is required.
setThrottleRegsTool     .. Tool for setting values of registers related to
                           throttling. Implemented as kernel module, includes
                           python wrappers.
                          *Make before deploy.
                          *Modules require KDIR of the sources of kernel they
                           run on. Use e.g. L4T R28.2.1 https://developer.nvidia.com/embedded/dlc/sources-r2821
                           Modify KDIR location in Makefile.
throttleRegsExperiments .. Python script utilizing previous two, to generate
                           data for visualization as charts.
                          *Run `python3 runMCThrottlingExperiments.py' on TX2.
text/plotting           .. Python script to generate charts from data.
                          *Run `toPdf.sh Results-directory'.



###

throttleOrig            .. Original tool for setting register values, now
                           replaced by setThrottleRegsTool.
readSetsOfRegsTool      .. Simple kernel module for outputing contents of
                           various sets of registers.
