#!/bin/bash

SCRIPT_DIR=`dirname $0`
cd ${SCRIPT_DIR}
tegra='tegra-ip-or-ssh-config-host'
deployPath='/home/nvidia/deploy/'


ssh nvidia@${tegra} -t "rm -r ${deployPath}/*"
scp -r . nvidia@${tegra}:${deployPath}
ssh nvidia@${tegra} -t "dbg=0 make -C ${deployPath}/bandwidthTests/"
