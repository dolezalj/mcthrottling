/*  
 *  hello-1.c - The simplest kernel module.
 */
#include <linux/module.h>	/* Needed by all modules */
#include <linux/kernel.h>	/* Needed for KERN_INFO */
#include <linux/random.h>
#include <linux/smp.h>
#include <linux/interrupt.h>
#include <linux/irqchip/arm-gic.h>
#include <linux/sched.h>   //wake_up_process()
#include <linux/kthread.h> //kthread_create(), kthread_run()
#include <linux/delay.h> //sleep
#include <linux/platform/tegra/mc.h> 
/*#include <linux/platform/tegra/mc-regs-t18x.h>*/
#include <linux/platform/tegra/mcerr_ecc_t18x.h> 
#include <linux/platform/tegra/tegra18_emc.h> 
#include <asm/io.h>
#include <linux/time.h>
#include <linux/highmem.h>
#include <linux/vmalloc.h>



//#include <err.h> //IS_ERR(), PTR_ERR()

#define DRVR_NAME "enable_arm_pmu"

#if !defined(__aarch64__)
	#error Module can only be compiled on ARM 64 machines.
#endif

/*
sudo -i
echo 0 > /sys/devices/system/cpu/cpu1/online
echo 0 > /sys/devices/system/cpu/cpu2/online
echo 0 > /sys/devices/system/cpu/cpu3/online
exit
* 
sudo reboot

 
 * */

/** -- Initialization & boilerplate ---------------------------------------- */

#define PERF_DEF_OPTS 		(1 | 16)
#define PERF_OPT_RESET_CYCLES 	(2 | 4)
#define PERF_OPT_DIV64 		(8)
#define ARMV8_PMCR_MASK         0x3f
#define ARMV8_PMCR_E            (1 << 0) /* Enable all counters */
#define ARMV8_PMCR_P            (1 << 1) /* Reset all counters */
#define ARMV8_PMCR_C            (1 << 2) /* Cycle counter reset */
#define ARMV8_PMCR_D            (1 << 3) /* CCNT counts every 64th cpu cycle */
#define ARMV8_PMCR_X            (1 << 4) /* Export to ETM */
#define ARMV8_PMCR_DP           (1 << 5) /* Disable CCNT if non-invasive debug*/
#define ARMV8_PMCR_N_SHIFT      11       /* Number of counters supported */
#define ARMV8_PMCR_N_MASK       0x1f

#define ARMV8_PMUSERENR_EN_EL0  (1 << 0) /* EL0 access enable */
#define ARMV8_PMUSERENR_CR      (1 << 2) /* Cycle counter read enable */
#define ARMV8_PMUSERENR_ER      (1 << 3) /* Event counter read enable */

#define ARMV8_PMCNTENSET_EL0_ENABLE (1<<31) /**< Enable Perf count reg */

#include "../bandwidthTests/bwTests.h"

#define ARB_MAX_OUTSTANDING_MAX_VALUE 0x1FF

static int testId = 0;
static int trot = 0xffffffff;
//static int outstand = 0x00000001;
static int outstand = ARB_MAX_OUTSTANDING_MAX_VALUE;
static int trotring = (THROT_RING0 | THROT_RING2 | THROT_RINGN); // 0x00000007;
static int nisomask0 = 0;
static int nisomask1 = 0;
static int ring0mask = 0;

module_param(testId, int,0);
module_param(trot, int,0);
module_param(outstand, int,0);
module_param(trotring, int,0);
module_param(nisomask0, int,0);
module_param(nisomask1, int,0);
module_param(ring0mask, int,0);

void latency_test(void);
void latency_test(void){
  // no throttling: i.e. throttling disabled
  mc_writel(0x0000,MC_EMEM_ARB_RING0_THROTTLE_MASK);
  mc_writel(0,MC_EMEM_ARB_NISO_THROTTLE_MASK);
  mc_writel(0,MC_EMEM_ARB_NISO_THROTTLE_MASK_1);
}

void trottle_test(void);
void trottle_test(void) {
  int trot1 = 0x0, trot3 = 0x0, trotniso = 0x0;
  int outstand1 = 0x0, outstand3 = 0x0, outstandniso = 0x0;
  if (trotring & THROT_RING0) {
    trot1 = trot;
    outstand1 = outstand | (0x80000000);
  }
  if (trotring & THROT_RING2) {
    trot3 = trot;
    outstand3 = outstand | (0x80000000);
  }
  if (trotring & THROT_RINGN) {
    trotniso = trot;
    outstandniso = outstand | (0x80000000);
  }
  // MC_EMEM_ARB_RING1_THROTTLE
  // 20:16 RING1_THROTTLE_CYCLES_HIGH: Cycles of throttle after each request when outstanding transaction count is greater than or equal to threshold.
  // 4:0   RING1_THROTTLE_CYCLES_LOW:  Cycles of throttle after each request when outstanding transaction count is below threshold. Suggested programming: (DIV==1)? 1 : 0.
  printk("MC_EMEM_ARB_RING1_THROTTLE: %x\n", trot1);
  mc_writel(trot1,MC_EMEM_ARB_RING1_THROTTLE);
  // MC_EMEM_ARB_RING3_THROTTLE (actually ring2)
  // 4:0   RING3_THROTTLE_CYCLES:      Cycles of throttle after each request.
  printk("MC_EMEM_ARB_RING3_THROTTLE: %x\n", trot3);
  mc_writel(trot3,MC_EMEM_ARB_RING3_THROTTLE);
  // MC_EMEM_ARB_NISO_THROTTLE
  // 31    NISO_THROTTLE_DISABLE_CNT_EVENT: [PMC] Count starts when "above_threshold" and "cnt_event" arrives. If "cnt_event" is disabled,
  //       only "above threshold" is used to start counting. In Parker, Ring2 arbiter's "snap_done" is used as "cnt_event".
  // 21:16 NISO_THROTTLE_CYCLES_HIGH:       [PMC] Cycles of throttle after each request when outstanding transaction count is greater than or equal to threshold.
  // 5:0   NISO_THROTTLE_CYCLES:            [PMC] Cycles of throttle after each request.
  printk("MC_EMEM_ARB_NISO_THROTTLE: %x\n", trotniso);
  mc_writel(trotniso,MC_EMEM_ARB_NISO_THROTTLE);
  // MC_EMEM_ARB_OUTSTANDING_REQ
  // 31    LIMIT_OUTSTANDING:             when ENABLED, the total number of requests in the arbiter is limited to the value in ARB_MAX_OUTSTANDING
  // 30    LIMIT_DURING_HOLDOFF_OVERRIDE: when DISABLED, override the limiting of transactions during hold-off to let requests flow freely
  // 24:16 ARB_OUTSTANDING_COUNT:         Current number of outstanding requests
  // 8:0   ARB_MAX_OUTSTANDING:           The maximum number of requests allowed in the arbiter when LIMIT_OUTSTANDING is set to ENABLE.
  printk("MC_EMEM_ARB_OUTSTANDING_REQ: %x\n", outstand1);
  mc_writel(outstand1,MC_EMEM_ARB_OUTSTANDING_REQ);
  // MC_EMEM_ARB_OUTSTANDING_REQ_RING3
  // 31    LIMIT_OUTSTANDING_RING3:             When ENABLED, requests into ring3 are throttled after the request count reaches ARB_MAX_OUTSTANDING_RING3.
  // 30    LIMIT_DURING_HOLDOFF_OVERRIDE_RING3: When DISABLED, overrides the limiting of ring3 transactions during holdoff to let requests flow freely.
  // 24:16 ARB_OUTSTANDING_COUNT_RING3:         Current number of outstanding requests
  // 8:0   ARB_MAX_OUTSTANDING_RING3:           The maximum number of requests allowed in the arbiter when LIMIT_OUTSTANDING_RING3 is set to ENABLE
  printk("MC_EMEM_ARB_OUTSTANDING_REQ_RING3: %x\n", outstand3);
  mc_writel(outstand3,MC_EMEM_ARB_OUTSTANDING_REQ_RING3);
  // MC_EMEM_ARB_OUTSTANDING_REQ_NISO
  // 31    LIMIT_OUTSTANDING_NISO:             When ENABLED, requests into NISO are throttled after the request count reaches ARB_MAX_OUTSTANDING_NISO.
  // 30    LIMIT_DURING_HOLDOFF_OVERRIDE_NISO: When DISABLED, overrides the limiting of NISO transactions during holdoff to let requests flow freely.
  // 24:16 ARB_OUTSTANDING_COUNT_NISO:         Current number of outstanding requests.
  // 8:0   ARB_MAX_OUTSTANDING_NISO:           The maximum number of requests allowed in the arbiter when LIMIT_OUTSTANDING_NISO is set to ENABLE.
  printk("MC_EMEM_ARB_OUTSTANDING_REQ_NISO: %x\n", outstandniso);
  mc_writel(outstandniso,MC_EMEM_ARB_OUTSTANDING_REQ_NISO);  
  printk("\n");
}

int tryMC(void);
int tryMC(){

  volatile uint32_t val,val2;

  local_irq_disable();
  //printk(KERN_INFO "ring 1 throttle 0x%x \n",mc_readl(MC_EMEM_ARB_RING1_THROTTLE));
  //printk(KERN_INFO "ring 1 outstanding 0x%x \n",mc_readl(MC_EMEM_ARB_OUTSTANDING_REQ));
  //printk(KERN_INFO "ring 1 test\n");
  //mc_writel(0x2,MC_TIMING_CONTROL_DBG);
  switch (testId){
    case 0:
      // add all partition clients to a single group for throttling
      mc_writel(0xffffffff,MC_EMEM_ARB_RING0_THROTTLE_MASK);
      mc_writel(0xffffffff,MC_EMEM_ARB_NISO_THROTTLE_MASK);
      mc_writel(0xffffffff,MC_EMEM_ARB_NISO_THROTTLE_MASK_1);
      trottle_test();
      break;
    case 1:
      //only GPU
      printk(KERN_INFO "only GPU\n");
      mc_writel(0,MC_EMEM_ARB_RING0_THROTTLE_MASK);
      mc_writel(1<<22,MC_EMEM_ARB_NISO_THROTTLE_MASK);
      mc_writel(1<<2,MC_EMEM_ARB_NISO_THROTTLE_MASK_1);
      trottle_test();
      break;
    case 2:
      //only GPU and CPU
      printk(KERN_INFO "only GPU and CPU\n");
      mc_writel(0x0001,MC_EMEM_ARB_RING0_THROTTLE_MASK);
      // mc_writel(0x0041,MC_EMEM_ARB_RING0_THROTTLE_MASK); // include also FTOP
      mc_writel(1<<22,MC_EMEM_ARB_NISO_THROTTLE_MASK);
      mc_writel(1<<2,MC_EMEM_ARB_NISO_THROTTLE_MASK_1);
      trottle_test();
      break;
    case 3:
      //only CPU
      printk(KERN_INFO "only CPU\n");
      //mc_writel(0x0001,MC_EMEM_ARB_RING0_THROTTLE_MASK);
      mc_writel(0x0041,MC_EMEM_ARB_RING0_THROTTLE_MASK); // include also FTOP
      mc_writel(0,MC_EMEM_ARB_NISO_THROTTLE_MASK);
      mc_writel(0,MC_EMEM_ARB_NISO_THROTTLE_MASK_1);
      trottle_test();
      break;
    case 4:
      printk(KERN_INFO "latency CPU\n");
      mc_writel(0x0080001a,MC_LATENCY_ALLOWANCE_GPU2_0);
      mc_writel(0x0080001a,MC_LATENCY_ALLOWANCE_GPU_0);
      mc_writel(trot,MC_LATENCY_ALLOWANCE_MPCORE_0);
      printk(KERN_INFO "latency reg 0x%x \n",mc_readl(MC_LATENCY_ALLOWANCE_MPCORE_0));
      latency_test();
      break;
    case 5:
      printk(KERN_INFO "latency GPU\n");
      mc_writel(trot,MC_LATENCY_ALLOWANCE_GPU2_0);
      mc_writel(trot,MC_LATENCY_ALLOWANCE_GPU_0);
      mc_writel(0x00800004,MC_LATENCY_ALLOWANCE_MPCORE_0);
      latency_test();
      break;
    case 6:
      printk(KERN_INFO "latency GPU and CPU\n");
      mc_writel(trot,MC_LATENCY_ALLOWANCE_GPU2_0);
      mc_writel(trot,MC_LATENCY_ALLOWANCE_GPU_0);
      mc_writel(trot,MC_LATENCY_ALLOWANCE_MPCORE_0);
      latency_test();
      break;
    case 7:
      //ring1 in ring0
      mc_writel(0x8080, MC_EMEM_ARB_RING0_THROTTLE_MASK);
      mc_writel(0,MC_EMEM_ARB_NISO_THROTTLE_MASK);
      mc_writel(0,MC_EMEM_ARB_NISO_THROTTLE_MASK_1);
      trottle_test();
      break;
    case 15:
      // set clients according to parameters provided
      mc_writel(ring0mask,MC_EMEM_ARB_RING0_THROTTLE_MASK);
      printk(KERN_INFO "MC_EMEM_ARB_RING0_THROTTLE_MASK: 0x%x\n", ring0mask);
      mc_writel(nisomask0,MC_EMEM_ARB_NISO_THROTTLE_MASK);
      printk(KERN_INFO "MC_EMEM_ARB_NISO_THROTTLE_MASK: 0x%x\n", nisomask0);
      mc_writel(nisomask1,MC_EMEM_ARB_NISO_THROTTLE_MASK_1);
      printk(KERN_INFO "MC_EMEM_ARB_NISO_THROTTLE_MASK_1: 0x%x\n", nisomask1);
      trottle_test();
      break;
    case 20:
//#define THROTTLE_CYCLES_HIGH_AND_LOW(HIGH,LOW)          ( ((HIGH&0x3f)<<16) | (LOW&0x3f) )
//#define THROTTLE_CYCLES_HIGH_AND_LOW(HIGH,LOW)          ( ((HIGH&0x3f)<<16) | (LOW&0x3f) )
//#define THROTTLE_CYCLES(CYCLES)                         ( CYCLES & 0x1f )
      // default values for the throttling
      mc_writel(0x80008041,  MC_EMEM_ARB_RING0_THROTTLE_MASK  );
      mc_writel(0x0,         MC_EMEM_ARB_NISO_THROTTLE_MASK   );
      mc_writel(0x0,         MC_EMEM_ARB_NISO_THROTTLE_MASK_1 );
      mc_writel(0x3f<<16,    MC_EMEM_ARB_RING1_THROTTLE       );
      mc_writel(0x0,         MC_EMEM_ARB_RING3_THROTTLE       );
      mc_writel(0x3f<<16,    MC_EMEM_ARB_NISO_THROTTLE        );
      mc_writel(1<<31|0x40,  MC_EMEM_ARB_OUTSTANDING_REQ      );
      mc_writel(1<<31|0x80,  MC_EMEM_ARB_OUTSTANDING_REQ_RING3);
      mc_writel(0x80000080,  MC_EMEM_ARB_OUTSTANDING_REQ_NISO );
      break;
  }
  
  
  //mc_writel(0x1,MC_TIMING_CONTROL_DBG);
  mc_writel(0x8,MC_TIMING_CONTROL_DBG);

  // ##### DDI0488C_cortex_a57_mpcore_r1p0_trm.pdf
  //Performance Monitors Control Register, EL0
  asm volatile("mrs %0, pmcr_el0" : "=r" (val));
  // enable all perf regs and reset
  asm volatile("msr pmcr_el0, %0" : : "r" (val|ARMV8_PMCR_E|ARMV8_PMCR_C));
  // Performance Monitors Count Enable Set Register {Architecture Reference Manual ARMv8}
  asm volatile("msr pmcntenset_el0, %0" : : "r" (0x8000000F));
  asm volatile("mrs %0, PMCCNTR_EL0" : "=r" (val2)); //read
  mc_writel(1,MC_TIMING_CONTROL);
  asm volatile("mrs %0, PMCCNTR_EL0" : "=r" (val)); //read
  printk(KERN_INFO "time of shadow switch, counter: %08ud\n",val-val2);
  
  //printk(KERN_INFO "ring 1 throttle 0x%x \n",mc_readl(MC_EMEM_ARB_RING1_THROTTLE));
    
  
  /*mc_writel(2,MC_EMEM_ARB_RING1_THROTTLE);
  //mc_writel(mereni[k],MC_STAT_EMC_FILTER_SET0_CLIENT_1);
  mc_writel(0,MC_STAT_CONTROL);      
  mc_writel(3,MC_STAT_CONTROL);
  mc_writel(2,MC_STAT_CONTROL);
  */
  local_irq_enable();
    
  
  return 0;
}


int init_module(void)
{
// ### see memory controller base address
//  int i;
//  extern void __iomem *mc;
//  extern void __iomem *mc_regs[MC_MAX_CHANNELS];
//
//  printk(KERN_INFO "mc ptr: %p \n", mc);
//  for(i=0;i<MC_MAX_CHANNELS;++i) {
//    printk(KERN_INFO "mc_regs[%d] ptr: %p \n", i, mc_regs[i]);
//  }
//  return 0;
  printk(KERN_INFO "start %d %d %d %d %d %d \n", testId, trot,outstand,nisomask0,nisomask1,ring0mask);
  tryMC();
  
	return 0;
}


void cleanup_module(void)
{
  //printk(KERN_INFO "konecny read 0x%x \n",mc_readl(MC_EMEM_ARB_RING0_THROTTLE_MASK));
  //printk(KERN_INFO "ring 1 throttle 0x%x \n",mc_readl(MC_EMEM_ARB_OUTSTANDING_REQ_RING3));
  //printk(KERN_INFO "ring 1 throttle 0x%x \n",mc_readl(MC_EMEM_ARB_OUTSTANDING_REQ_NISO));
}

MODULE_LICENSE("GPL");
