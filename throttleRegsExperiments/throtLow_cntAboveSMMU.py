mcScenarios = {
  'throtLow_r02N_SMMU'       : {
     'iter_throt_cycles'                  : [0, 1, 2, 4, 8, 16, 31],
     'apply_iter_throt_to'                : ['r0_low', 'r3_low', 'niso_low'],

     'r1_limit_outstanding'               : True,
     'r1_limit_during_holdoff_override'   : False,
     'r1_max_outstanding_requests_d'      : 1,

     'r0_arb_outstanding_count_above_SMMU': True,
     'clientSets' : ["", "GK GK2"],#, "MPCORER", "FTOP", "MPCORER FTOP", "RING1 RING1_OUTPUT"],
     'handler' : 'mcMultipleThrotCycles',
  },
  'throtLow_r02_SMMU'        : {
     'iter_throt_cycles'                  : [0, 1, 2, 4, 8, 16, 31],
     'apply_iter_throt_to'                : ['r0_low', 'r3_low'],

     'r1_limit_outstanding'               : True,
     'r1_limit_during_holdoff_override'   : False,
     'r1_max_outstanding_requests_d'      : 1,

     'r0_arb_outstanding_count_above_SMMU': True,
     'clientSets' : ["", "GK GK2"],#, "MPCORER", "FTOP", "MPCORER FTOP", "RING1 RING1_OUTPUT"],
     'handler' : 'mcMultipleThrotCycles',
  },
  'throtLow_r0N_SMMU'        : {
     'iter_throt_cycles'                  : [0, 1, 2, 4, 8, 16, 31],
     'apply_iter_throt_to'                : ['r0_low', 'niso_low'],

     'r1_limit_outstanding'               : True,
     'r1_limit_during_holdoff_override'   : False,
     'r1_max_outstanding_requests_d'      : 1,

     'r0_arb_outstanding_count_above_SMMU': True,
     'clientSets' : ["", "GK GK2"],#, "MPCORER", "FTOP", "MPCORER FTOP", "RING1 RING1_OUTPUT"],
     'handler' : 'mcMultipleThrotCycles',
  },
  'throtLow_r2N_SMMU'        : {
     'iter_throt_cycles'                  : [0, 1, 2, 4, 8, 16, 31],
     'apply_iter_throt_to'                : ['r3_low', 'niso_low'],

     'r1_limit_outstanding'               : True,
     'r1_limit_during_holdoff_override'   : False,
     'r1_max_outstanding_requests_d'      : 1,

     'r0_arb_outstanding_count_above_SMMU': True,
     'clientSets' : ["", "GK GK2"],#, "MPCORER", "FTOP", "MPCORER FTOP", "RING1 RING1_OUTPUT"],
     'handler' : 'mcMultipleThrotCycles',
  },
  'throtLow_r0_SMMU'         : {
     'iter_throt_cycles'                  : [0, 1, 2, 4, 8, 16, 31],
     'apply_iter_throt_to'                : ['r0_low'],

     'r1_limit_outstanding'               : True,
     'r1_limit_during_holdoff_override'   : False,
     'r1_max_outstanding_requests_d'      : 1,

     'r0_arb_outstanding_count_above_SMMU': True,
     'clientSets' : ["", "GK GK2"],#, "MPCORER", "FTOP", "MPCORER FTOP", "RING1 RING1_OUTPUT"],
     'handler' : 'mcMultipleThrotCycles',
  },
  'throtLow_r2_SMMU'         : {
     'iter_throt_cycles'                  : [0, 1, 2, 4, 8, 16, 31],
     'apply_iter_throt_to'                : ['r3_low'],

     'r3_limit_outstanding'               : True,
     'r3_limit_during_holdoff_override'   : False,
     'r3_max_outstanding_requests_d'      : 1,

     'r0_arb_outstanding_count_above_SMMU': True,
     'clientSets' : ["", "GK GK2"],#, "MPCORER", "FTOP", "MPCORER FTOP", "RING1 RING1_OUTPUT"],
     'handler' : 'mcMultipleThrotCycles',
  },
  'throtLow_niso_SMMU'       : {
     'iter_throt_cycles'                  : [0, 1, 2, 4, 8, 16, 31],
     'apply_iter_throt_to'                : ['niso_low'],

     'niso_limit_outstanding'             : True,
     'niso_limit_during_holdoff_override' : False,
     'niso_max_outstanding_requests_d'    : 1,

     'r0_arb_outstanding_count_above_SMMU': True,
     'clientSets' : ["", "GK GK2"],#, "MPCORER", "FTOP", "MPCORER FTOP", "RING1 RING1_OUTPUT"],
     'handler' : 'mcMultipleThrotCycles',
  },
}

scenarios = {
#  'mGPU_iDenver2_r02N_tlow_SMMU'    : {
#                              'bw' : 'mPascal_iDenver2',
#                              'mc' : 'throtLow_r02N_SMMU',
#                              'description' : '',
#                            },
#  'mGPU_iA57_r02N_tlow_SMMU'        : {
#                              'bw' : 'mPascal_iA57',
#                              'mc' : 'throtLow_r02N_SMMU',
#                              'description' : '',
#                            },
#  'mGPU_iDenver2_r02_tlow_SMMU'     : {
#                              'bw' : 'mPascal_iDenver2',
#                              'mc' : 'throtLow_r02_SMMU',
#                              'description' : '',
#                            },
#  'mGPU_iA57_r02_tlow_SMMU'         : {
#                              'bw' : 'mPascal_iA57',
#                              'mc' : 'throtLow_r02_SMMU',
#                              'description' : '',
#                            },
#  'mGPU_iDenver2_r0N_tlow_SMMU'     : {
#                              'bw' : 'mPascal_iDenver2',
#                              'mc' : 'throtLow_r0N_SMMU',
#                              'description' : '',
#                            },
#  'mGPU_iA57_r0N_tlow_SMMU'         : {
#                              'bw' : 'mPascal_iA57',
#                              'mc' : 'throtLow_r0N_SMMU',
#                              'description' : '',
#                            },
#  'mGPU_iDenver2_r2N_tlow_SMMU'     : {
#                              'bw' : 'mPascal_iDenver2',
#                              'mc' : 'throtLow_r2N_SMMU',
#                              'description' : '',
#                            },
#  'mGPU_iA57_r2N_tlow_SMMU'         : {
#                              'bw' : 'mPascal_iA57',
#                              'mc' : 'throtLow_r2N_SMMU',
#                              'description' : '',
#                            },
  'mGPU_iDenver2_r0_tlow_SMMU'      : {
                              'bw' : 'mPascal_iDenver2',
                              'mc' : 'throtLow_r0_SMMU',
                              'description' : '',
                            },
#  'mGPU_iA57_r0_tlow_SMMU'          : {
#                              'bw' : 'mPascal_iA57',
#                              'mc' : 'throtLow_r0_SMMU',
#                              'description' : '',
#                            },
  'mGPU_iDenver2_r2_tlow_SMMU'      : {
                              'bw' : 'mPascal_iDenver2',
                              'mc' : 'throtLow_r2_SMMU',
                              'description' : '',
                            },
#  'mGPU_iA57_r2_tlow_SMMU'          : {
#                              'bw' : 'mPascal_iA57',
#                              'mc' : 'throtLow_r2_SMMU',
#                              'description' : '',
#                            },
  'mGPU_iDenver2_niso_tlow_SMMU'    : {
                              'bw' : 'mPascal_iDenver2',
                              'mc' : 'throtLow_niso_SMMU',
                              'description' : '',
                            },
#  'mGPU_iA57_niso_tlow_SMMU'        : {
#                              'bw' : 'mPascal_iA57',
#                              'mc' : 'throtLow_niso_SMMU',
#                              'description' : '',
#                            },
}
