mcScenarios = {
  'throtLow_niso_reg_outst' : {
     'iter_throt_cycles'                  : [0, 1, 2, 4, 8, 16, 31],
     'apply_iter_throt_to'                : ['niso_low'],

     'niso_limit_outstanding'             : True,
     'niso_limit_during_holdoff_override' : False,
     'niso_max_outstanding_requests_d'    : 0x5,

#     'r0_arb_outstanding_count_above_SMMU': False,
     'clientSets' : ["", "GK GK2"],
     'handler' : 'mcMultipleThrotCycles',
  },
  'throtLow_niso_high_outst' : {
     'iter_throt_cycles'                  : [0, 1, 2, 4, 8, 16, 31],
     'apply_iter_throt_to'                : ['niso_low'],

     'niso_limit_outstanding'             : True,
     'niso_limit_during_holdoff_override' : False,
     'niso_max_outstanding_requests_d'    : 0x100,

#     'r0_arb_outstanding_count_above_SMMU': False,
     'clientSets' : ["", "GK GK2"],
     'handler' : 'mcMultipleThrotCycles',
  },
  'throtLow_niso_reg_outst_holdoff' : {
     'iter_throt_cycles'                  : [0, 1, 2, 4, 8, 16, 31],
     'apply_iter_throt_to'                : ['niso_low'],

     'niso_limit_outstanding'             : True,
     'niso_limit_during_holdoff_override' : True,
     'niso_max_outstanding_requests_d'    : 0x5,

#     'r0_arb_outstanding_count_above_SMMU': False,
     'clientSets' : ["", "GK GK2"],
     'handler' : 'mcMultipleThrotCycles',
  },
  'throtLow_niso_high_outst_holdoff' : {
     'iter_throt_cycles'                  : [0, 1, 2, 4, 8, 16, 31],
     'apply_iter_throt_to'                : ['niso_low'],

     'niso_limit_outstanding'             : True,
     'niso_limit_during_holdoff_override' : True,
     'niso_max_outstanding_requests_d'    : 0x100,

#     'r0_arb_outstanding_count_above_SMMU': False,
     'clientSets' : ["", "GK GK2"],
     'handler' : 'mcMultipleThrotCycles',
  },
}

scenarios = {
  'mGPU_Denver2_niso_only_reg'      : {
                                    'bw' : 'mPascal_iDenver2',
                                    'mc' : 'throtLow_niso_reg_outst',
                                    'description' : '',
                                  },
  'mGPU_Denver2_niso_only_ho'       : {
                                    'bw' : 'mPascal_iDenver2',
                                    'mc' : 'throtLow_niso_high_outst',
                                    'description' : '',
                                  },
  'mGPU_Denver2_niso_only_reg_hold' : {
                                    'bw' : 'mPascal_iDenver2',
                                    'mc' : 'throtLow_niso_reg_outst_holdoff',
                                    'description' : '',
                                  },
  'mGPU_Denver2_niso_only_ho_hold'  : {
                                    'bw' : 'mPascal_iDenver2',
                                    'mc' : 'throtLow_niso_high_outst_holdoff',
                                    'description' : '',
                                  },
}
