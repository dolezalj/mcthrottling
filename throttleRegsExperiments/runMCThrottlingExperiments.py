#!/usr/bin/python3

# This script runs target program [bwTests] with different parameters.
# This is meant to be run on the tested platform.

import os
import sys
import subprocess
import MC
this = sys.modules[__name__]

# tests collections
import                          originalTests
import             originalTests_HighOutstand
import                               throtLow
import                              throtHigh

import                     throt_cntAboveSMMU
import                  throtLow_cntAboveSMMU
import                 throtHigh_cntAboveSMMU
import thIt_bits_nisoDisCntEvent_cntAboveSMMU
thIt_bits_nisoDisCntEvent_cntAboveSMMU.init()
import                         throt_noClient
import             throtLow_NISO_HighOutstand

jetsonClocksScript='/home/nvidia/jetson_clocks.sh'
clocksConfFile='jetson-tmp.conf'
subprocess.run(['sudo', jetsonClocksScript, '--store', clocksConfFile])
subprocess.run(['sudo', 'chown', 'nvidia:nvidia', clocksConfFile])
subprocess.run(['sudo', jetsonClocksScript])
subprocess.run(['sudo', '../enableAllCPUs.sh'])
subprocess.run(['sleep', '5'])

resultsDir = "Results-MCThrottlingExperiments"
scenarioResultsDir = ""
filename = ""
if not os.path.exists(resultsDir):
  os.makedirs(resultsDir)

bwargs = []

def mcMultipleThrotCycles(mcScenario, outfile):
  for throt in mcScenario['iter_throt_cycles']:
    if 'r0_high' in mcScenario['apply_iter_throt_to']:
      MC.setRing0throttleCyclesHigh(throt)
    if 'r0_low' in mcScenario['apply_iter_throt_to']:
      MC.setRing0throttleCyclesLow(throt)
    if 'r3_high' in mcScenario['apply_iter_throt_to']:
      MC.setRing3throttleCyclesHigh(throt)
    if 'r3_low' in mcScenario['apply_iter_throt_to']:
      MC.setRing3throttleCycles(throt)
    if 'niso_high' in mcScenario['apply_iter_throt_to']:
      MC.setNISOthrottleCyclesHigh(throt)
    if 'niso_low' in mcScenario['apply_iter_throt_to']:
      MC.setNISOthrottleCycles(throt)
    outfile.write("## "+' '.join(MC.getThrottleRegLocVars())+"\n")
    outfile.write("T"+str(throt)+" ")
    outfile.flush()
    MC.writeOutThrottleRegisters()
    subprocess.run(bwargs, stdout=outfile)

def mcMultipleOutstandReqs(mcScenario, outfile):
  for outReq in mcScenario['max_outstand_reqs']:
    if 'r1' in mcScenario['apply_outstand_reqs_to']:
      MC.setRing1maxOutstanding(outReq)
    if 'r3' in mcScenario['apply_outstand_reqs_to']:
      MC.setRing3maxOutstanding(outReq)
    if 'niso' in mcScenario['apply_outstand_reqs_to']:
      MC.setNISOmaxOutstanding(outReq)
    outfile.write("## "+' '.join(MC.getThrottleRegLocVars())+"\n")
    outfile.write("O"+str(outReq)+" ")
    MC.writeOutThrottleRegisters()
    subprocess.run(bwargs, stdout=outfile)

# throt_cycles : values 0-31 are certainly defined on all rings of the memory controller; some rings accept values up to 63
# outstanding_requests : [0, 1, 2, 4, 8, 16, 32, 64, 128, 256, 511]
# _d : default values, might get overriden by iter_
# handler: function that decides how will be the data processed (e.g. which iter_ will be applied)
mcScenarios = {
  'exampleAllParamsListing':{
     'iter_throt_cycles' : [0, 1, 2, 4, 8, 16, 31],
     'apply_iter_throt_to' : ['r0_high', 'r0_low', 'r3_high', 'r3_low', 'niso_high', 'niso_low'],

     'r0_throt_cycles_high_d'  : 0,
     'r0_throt_cycles_low_d'   : 0,
     'r3_throt_cycles_high_d'  : 0,
     'r3_throt_cycles_low_d'   : 0,
     'niso_throt_cycles_high_d': 0,
     'niso_throt_cycles_low_d' : 0,
     'niso_throttle_disable_cnt_event' : False,

     'iter_max_outstand_reqs' : [0, 1, 2, 4, 8, 16, 32],
     'apply_iter_outstand_to' : ['r1', 'r3', 'niso'],

     'r1_max_outstanding_requests_d'      : 1,
     'r1_limit_outstanding'               : True,
     'r1_limit_during_holdoff_override'   : True,
     'r3_max_outstanding_requests_d'      : 1,
     'r3_limit_outstanding'               : True,
     'r3_limit_during_holdoff_override'   : True,
     'niso_max_outstanding_requests_d'    : 1,
     'niso_limit_outstanding'             : True,
     'niso_limit_during_holdoff_override' : True,

     'clientSets'             : ["", "GK GK2"],
     'r0_arb_outstanding_count_above_SMMU': True,
     'handler'                : lambda fnc : print("unimplemented"),
  },
  'throt_clients'             : {
     'iter_throt_cycles' : [0, 1, 2, 4, 8, 16, 31],
     'apply_iter_throt_to' : ['niso_high', 'niso_low'],
     'r1_max_outstanding_requests_d'      : 0,
     'r1_limit_outstanding'               : False,
     'r1_limit_during_holdoff_override'   : False,
     'r3_max_outstanding_requests_d'      : 0,
     'r3_limit_outstanding'               : False,
     'r3_limit_during_holdoff_override'   : False,
     'niso_max_outstanding_requests_d'    : 1,
     'niso_limit_outstanding'             : True,
     'niso_limit_during_holdoff_override' : False,
     'niso_max_outstanding_requests' : [1],
     'clientSets' : ["", "GK GK2"],
     'handler' : mcMultipleThrotCycles,
  },
  'throt_clients_63_hcyc'     : {
     'niso_throt_cycles_high_d': 63,
     'iter_throt_cycles' : [0, 1, 2, 4, 8, 16, 31],
     'apply_iter_throt_to' : ['niso_low'],
     'r1_max_outstanding_requests_d'      : 0,
     'r1_limit_outstanding'               : False,
     'r1_limit_during_holdoff_override'   : False,
     'r3_max_outstanding_requests_d'      : 0,
     'r3_limit_outstanding'               : False,
     'r3_limit_during_holdoff_override'   : False,
     'niso_max_outstanding_requests_d'    : 1,
     'niso_limit_outstanding'             : True,
     'niso_limit_during_holdoff_override' : False,
     'niso_max_outstanding_requests' : [1],
     'clientSets' : ["", "GK GK2"],
     'handler' : mcMultipleThrotCycles,
  },
  'throt_clients_240_outst'   : {
     'iter_throt_cycles' : [0, 1, 2, 4, 8, 16, 31],
     'apply_iter_throt_to' : ['niso_high', 'niso_low'],
     'r1_max_outstanding_requests_d'      : 0,
     'r1_limit_outstanding'               : False,
     'r1_limit_during_holdoff_override'   : False,
     'r3_max_outstanding_requests_d'      : 0,
     'r3_limit_outstanding'               : False,
     'r3_limit_during_holdoff_override'   : False,
     'niso_max_outstanding_requests_d'    : 240,
     'niso_limit_outstanding'             : True,
     'niso_limit_during_holdoff_override' : False,
     'niso_max_outstanding_requests' : [1],
     'clientSets' : ["", "GK GK2"],
     'handler' : mcMultipleThrotCycles,
  },
}
mcScenarios.update(                         originalTests.mcScenarios)
mcScenarios.update(            originalTests_HighOutstand.mcScenarios)
mcScenarios.update(                              throtLow.mcScenarios)
mcScenarios.update(                             throtHigh.mcScenarios)
mcScenarios.update(                    throt_cntAboveSMMU.mcScenarios)
mcScenarios.update(                 throtLow_cntAboveSMMU.mcScenarios)
mcScenarios.update(                throtHigh_cntAboveSMMU.mcScenarios)
mcScenarios.update(thIt_bits_nisoDisCntEvent_cntAboveSMMU.mcScenarios)
mcScenarios.update(                        throt_noClient.mcScenarios)
mcScenarios.update(            throtLow_NISO_HighOutstand.mcScenarios)

bwScenarios = {
  'mPascal_iDenver2'      : [
                              "measureDenver2",
                              "measurePascal",
                              "measurePascal interfDenver2 threads1",
                              "measurePascal interfDenver2 threads2",
                            ],
  'mPascal_iA57'          : [
                              "measureA57",
                              "measurePascal",
                              "measurePascal interfA57 threads1",
                              "measurePascal interfA57 threads2",
                            ],
}

scenarios = {
  'mGPU_iDenver2_hcyc63'  : {
                              'bw' : 'mPascal_iDenver2',
                              'mc' : 'throt_clients_63_hcyc',
                              'description' : '',
                            },
  'mGPU_iDenver2_outs240' : {
                              'bw' : 'mPascal_iDenver2',
                              'mc' : 'throt_clients_240_outst',
                              'description' : '',
                            },
  'mGPU_iDenver2_mcOrig'  : {
                              'bw' : 'mPascal_iDenver2',
                              'mc' : 'throt_clients',
                              'description' : '',
                            },
  'mGPU_iA57_mcOrig'      : {
                              'bw' : 'mPascal_iA57',
                              'mc' : 'throt_clients',
                              'description' : '',
                            },
}
scenarios.update(                         originalTests.scenarios)
scenarios.update(            originalTests_HighOutstand.scenarios)
scenarios.update(                              throtLow.scenarios)
scenarios.update(                             throtHigh.scenarios)
scenarios.update(                    throt_cntAboveSMMU.scenarios)
scenarios.update(                 throtLow_cntAboveSMMU.scenarios)
scenarios.update(                throtHigh_cntAboveSMMU.scenarios)
scenarios.update(thIt_bits_nisoDisCntEvent_cntAboveSMMU.scenarios)
scenarios.update(                        throt_noClient.scenarios)
scenarios.update(            throtLow_NISO_HighOutstand.scenarios)

runScenarios = []#['mGPU_iDenver2_hcyc63', 'mGPU_iDenver2_outs240']

#runScenarios += list(                         originalTests.scenarios.keys())
#runScenarios += list(            originalTests_HighOutstand.scenarios.keys())
#runScenarios += list(                              throtLow.scenarios.keys())
#runScenarios += list(                             throtHigh.scenarios.keys())
#runScenarios += list(                    throt_cntAboveSMMU.scenarios.keys())
#runScenarios += list(                 throtLow_cntAboveSMMU.scenarios.keys())
#runScenarios += list(                throtHigh_cntAboveSMMU.scenarios.keys())
#runScenarios += list(thIt_bits_nisoDisCntEvent_cntAboveSMMU.scenarios.keys())
runScenarios += list(                        throt_noClient.scenarios.keys())
#runScenarios += list(            throtLow_NISO_HighOutstand.scenarios.keys())

for scenario in runScenarios:
  currentDescription = scenarios[scenario]['description']
  currentMCscenario = mcScenarios[scenarios[scenario]['mc']]
  currentBWscenario = bwScenarios[scenarios[scenario]['bw']]

  scenarioResultsDir = resultsDir + "/" + scenarios[scenario]['bw']+"_"+scenarios[scenario]['mc']
  if not os.path.exists(scenarioResultsDir):
    os.makedirs(scenarioResultsDir)

  # set up MC
  for cs in currentMCscenario['clientSets']:
    MC.clearThrottleRegLocVars()
    MC.setRing0throttleCyclesHigh(currentMCscenario.get('r0_throt_cycles_high_d', 0))
    MC.setRing0throttleCyclesLow(currentMCscenario.get('r0_throt_cycles_low_d', 0))
    MC.setRing3throttleCyclesHigh(currentMCscenario.get('r3_throt_cycles_high_d', 0))
    MC.setRing3throttleCycles(currentMCscenario.get('r3_throt_cycles_low_d', 0))
    MC.setNISOthrottleCyclesHigh(currentMCscenario.get('niso_throt_cycles_high_d', 0))
    MC.setNISOthrottleCycles(currentMCscenario.get('niso_throt_cycles_low_d', 0))
    MC.setNISOthrottleDisableCntEvent(currentMCscenario.get('niso_throttle_disable_cnt_event', False))
    MC.setRing1maxOutstanding(currentMCscenario.get('r1_max_outstanding_requests_d', 0))
    MC.setRing1limitOutstanding(currentMCscenario.get('r1_limit_outstanding', False))
    MC.setRing1limitDuringHoldoffOverride(currentMCscenario.get('r1_limit_during_holdoff_override', False))
    MC.setRing3maxOutstanding(currentMCscenario.get('r3_max_outstanding_requests_d', 0))
    MC.setRing3limitOutstanding(currentMCscenario.get('r3_limit_outstanding', False))
    MC.setRing3limitDuringHoldoffOverride(currentMCscenario.get('r3_limit_during_holdoff_override', False))
    MC.setNISOmaxOutstanding(currentMCscenario.get('niso_max_outstanding_requests_d', 0))
    MC.setNISOlimitOutstanding(currentMCscenario.get('niso_limit_outstanding', False))
    MC.setNISOlimitDuringHoldoffOverride(currentMCscenario.get('niso_limit_during_holdoff_override', False))
    MC.setRing0arbOutstandingCountAboveSMMU(currentMCscenario.get('r0_arb_outstanding_count_above_SMMU', False))
    for s in cs.split():
      if(MC.isR0client(s)):
        MC.addRing0client(MC.client(s));
      if(MC.isN0client(s)):
        MC.addNISOclient(MC.client(s));
      if(MC.isN1client(s)):
        MC.addNISO1client(MC.client(s));

    filename = "_" if cs == "" else cs.replace(' ', '_')
    tmppath = os.path.join(scenarioResultsDir, "tmp.log")
    tmpfile = open(tmppath, 'w')

    for bwMeasurementCase in currentBWscenario:
      bwargs = ["../bandwidthTests/bwTests"] + bwMeasurementCase.split() + ["measRpts1"]
      caseHeader="\n# "+bwMeasurementCase+"\n"
      tmpfile.write(caseHeader)
      handler = currentMCscenario.get('handler')
      if handler:
        if isinstance(handler, str):
          getattr(this, handler)(currentMCscenario, tmpfile)
        else:
          currentMCscenario['handler'](currentMCscenario, tmpfile)
    tmpfile.close();
    dstpath = os.path.join(scenarioResultsDir, filename+".log")
    print("Writing results to: "+dstpath)
    # tmpfile approach allows filename to be modified along the way
    os.rename(tmppath, dstpath)

subprocess.run(['sudo', jetsonClocksScript, '--restore', clocksConfFile])
subprocess.run(['sudo', 'rm', clocksConfFile])
