import sys
import subprocess
this = sys.modules[__name__]

this.ring0_throttle_mask   = 0x0
this.niso_throttle_mask    = 0x0
this.niso_throttle_mask_1  = 0x0
this.ring1_throttle        = 0x0
this.ring3_throttle        = 0x0
this.niso_throttle         = 0x0
this.outstanding_req       = 0x0
this.outstanding_req_ring3 = 0x0
this.outstanding_req_niso  = 0x0
def clearThrottleRegLocVars():
  this.ring0_throttle_mask   = 0x0
  this.niso_throttle_mask    = 0x0
  this.niso_throttle_mask_1  = 0x0
  this.ring1_throttle        = 0x0
  this.ring3_throttle        = 0x0
  this.niso_throttle         = 0x0
  this.outstanding_req       = 0x0
  this.outstanding_req_ring3 = 0x0
  this.outstanding_req_niso  = 0x0
def getThrottleRegLocVars():
  return [
    "%s0x%08.2X" % ("ring0_throttle_mask="  , this.ring0_throttle_mask  ),
    "%s0x%08.2X" % ("niso_throttle_mask="   , this.niso_throttle_mask   ),
    "%s0x%08.2X" % ("niso_throttle_mask_1=" , this.niso_throttle_mask_1 ),
    "%s0x%08.2X" % ("ring1_throttle="       , this.ring1_throttle       ),
    "%s0x%08.2X" % ("ring3_throttle="       , this.ring3_throttle       ),
    "%s0x%08.2X" % ("niso_throttle="        , this.niso_throttle        ),
    "%s0x%08.2X" % ("outstanding_req="      , this.outstanding_req      ),
    "%s0x%08.2X" % ("outstanding_req_ring3=", this.outstanding_req_ring3),
    "%s0x%08.2X" % ("outstanding_req_niso=" , this.outstanding_req_niso )
    ]
def writeOutThrottleRegisters():
  subprocess.run(["../setThrottleRegsTool/writeThrottleRegs.sh"]+getThrottleRegLocVars())

### Clients to throttle ###
def isR0client(c):
  return {
    'RING1_OUTPUT': True,
    'RING1'       : True,
    'FTOP'        : True,
    'PTC'         : True,
    'MPCORER'     : True,
  }.get(c.strip(), False)

def isN0client(c):
  return {
    'VICPC': True,
    'USBD' : True,
    'HOST' : True,
    'SD'   : True,
    'GK'   : True,
    'MSE'  : True,
    'USBX' : True,
    'SAX'  : True,
    'PCX'  : True,
    'AVP'  : True,
    'APB'  : True,
    'AHB'  : True,
  }.get(c.strip(), False)

def isN1client(c):
  return {
    'DFD'  : True,
    'HDAPC': True,
    'SDM'  : True,
    'GK2'  : True,
    'JPG'  : True,
  }.get(c.strip(), False)

def client(c):
  return {
    # MC_EMEM_ARB_RING0_THROTTLE_MASK: RING0_THROTTLE_MASK_
    'RING1_OUTPUT': 1 << 15,
    'RING1'       : 1 << 7 ,
    'FTOP'        : 1 << 6 ,
    'PTC'         : 1 << 2 ,
    'MPCORER'     : 1 << 0 ,
    # MC_EMEM_ARB_NISO_THROTTLE_MASK_0: NISO_THROTTLE_MASK_
    'VICPC': 1 <<  29,
    'USBD' : 1 <<  28,
    'HOST' : 1 <<  27,
    'SD'   : 1 <<  23,
    'GK'   : 1 <<  22,
    'MSE'  : 1 <<  19,
    'USBX' : 1 <<  17,
    'SAX'  : 1 <<  9 ,
    'PCX'  : 1 <<  7 ,
    'AVP'  : 1 <<  3 ,
    'APB'  : 1 <<  2 ,
    'AHB'  : 1 <<  1 ,
    # MC_EMEM_ARB_NISO_THROTTLE_MASK_1_0: NISO_THROTTLE_MASK_
    'DFD'  : 1 <<  5 ,
    'HDAPC': 1 <<  4 ,
    'SDM'  : 1 <<  3 ,
    'GK2'  : 1 <<  2 ,
    'JPG'  : 1 <<  1 ,
    # on empty string return nothing
    ''     : 0       ,
  }[c.strip()] # we want this to fail on bad value => no default
  #}.get(c, 0) # 0 is default if c not found

def setRing0arbOutstandingCountAboveSMMU(enable):
  if (enable):
    this.ring0_throttle_mask |= 1 << 31
  else:
    this.ring0_throttle_mask &= (~(1 << 31) & 0xFFFFFFFF)

def addRing0client(clientMask):
  this.ring0_throttle_mask |= clientMask

def addNISOclient(clientMask):
  this.niso_throttle_mask |= clientMask

def addNISO1client(clientMask):
  this.niso_throttle_mask_1 |= clientMask


### Throttle cycle counts ###
def setRing0throttleCyclesHigh(cyclesCount):
  if(cyclesCount > 0x1F):
    print("setRing0throttleCyclesHigh received cyclesCount > 0x1F: %d\n" % (cyclesCount), file=sys.stderr)
  this.ring1_throttle &= (~(0x1F                << 16) & 0xFFFFFFFF)
  this.ring1_throttle |= (((0x1F & cyclesCount) << 16) & 0xFFFFFFFF)
def setRing0throttleCyclesLow(cyclesCount):
  if(cyclesCount > 0x1F):
    print("setRing0throttleCyclesLow received cyclesCount > 0x1F: %d\n" % (cyclesCount), file=sys.stderr)
  this.ring1_throttle &= (~(0x1F                << 0) & 0xFFFFFFFF)
  this.ring1_throttle |= (((0x1F & cyclesCount) << 0) & 0xFFFFFFFF)
# The setRing3throttleCyclesHigh has probably no effect as it is not explicitly defined in the Parker_TRM
def setRing3throttleCyclesHigh(cyclesCount):
  if(cyclesCount > 0x1F):
    print("setRing3throttleCyclesHigh received cyclesCount > 0x1F: %d\n" % (cyclesCount), file=sys.stderr)
  this.ring3_throttle &= (~(0x1F                << 16) & 0xFFFFFFFF)
  this.ring3_throttle |= (((0x1F & cyclesCount) << 16) & 0xFFFFFFFF)
def setRing3throttleCycles(cyclesCount):
  if(cyclesCount > 0x1F):
    print("setRing3throttleCycles received cyclesCount > 0x1F: %d\n" % (cyclesCount), file=sys.stderr)
  this.ring3_throttle &= (~(0x1F                << 0) & 0xFFFFFFFF)
  this.ring3_throttle |= (((0x1F & cyclesCount) << 0) & 0xFFFFFFFF)
def setNISOthrottleCyclesHigh(cyclesCount):
  if(cyclesCount > 0x3F):
    print("setNISOthrottleCyclesHigh received cyclesCount > 0x3F: %d\n" % (cyclesCount), file=sys.stderr)
  this.niso_throttle &= (~(0x3F                << 16) & 0xFFFFFFFF)
  this.niso_throttle |= (((0x3F & cyclesCount) << 16) & 0xFFFFFFFF)
def setNISOthrottleCycles(cyclesCount):
  if(cyclesCount > 0x3F):
    print("setNISOthrottleCycles received cyclesCount > 0x3F: %d\n" % (cyclesCount), file=sys.stderr)
  this.niso_throttle &= (~(0x3F                << 0) & 0xFFFFFFFF)
  this.niso_throttle |= (((0x3F & cyclesCount) << 0) & 0xFFFFFFFF)

def setNISOthrottleDisableCntEvent(enable):
  if (enable):
    this.ring0_throttle_mask |= 1 << 31
  else:
    this.ring0_throttle_mask &= (~(1 << 31) & 0xFFFFFFFF)

### Throttle outstanding ###
def setRing1maxOutstanding(requestsCount):
  if(requestsCount > 0x1FF):
    print("setRing1maxOutstanding received requestsCount > 0x1FF: %d\n" % (requestsCount), file=sys.stderr)
  this.outstanding_req &= (~(0x1FF                << 0) & 0xFFFFFFFF)
  this.outstanding_req |= (((0x1FF & requestsCount) << 0) & 0xFFFFFFFF)

def setRing1limitOutstanding(enable):
  if (enable):
    this.outstanding_req |= 1 << 31
  else:
    this.outstanding_req &= (~(1 << 31) & 0xFFFFFFFF)
def setRing1limitDuringHoldoffOverride(enable):
  if (enable):
    this.outstanding_req |= 1 << 30
  else:
    this.outstanding_req &= (~(1 << 30) & 0xFFFFFFFF)

def setRing3maxOutstanding(requestsCount):
  if(requestsCount > 0x1FF):
    print("setRing3maxOutstanding received requestsCount > 0x1FF: %d\n" % (requestsCount), file=sys.stderr)
  this.outstanding_req_ring3 &= (~(0x1FF                << 0) & 0xFFFFFFFF)
  this.outstanding_req_ring3 |= (((0x1FF & requestsCount) << 0) & 0xFFFFFFFF)

def setRing3limitOutstanding(enable):
  if (enable):
    this.outstanding_req_ring3 |= 1 << 31
  else:
    this.outstanding_req_ring3 &= (~(1 << 31) & 0xFFFFFFFF)
def setRing3limitDuringHoldoffOverride(enable):
  if (enable):
    this.outstanding_req_ring3 |= 1 << 30
  else:
    this.outstanding_req_ring3 &= (~(1 << 30) & 0xFFFFFFFF)

def setNISOmaxOutstanding(requestsCount):
  if(requestsCount > 0x1FF):
    print("setNISOmaxOutstanding received requestsCount > 0x1FF: %d\n" % (requestsCount), file=sys.stderr)
  this.outstanding_req_niso &= (~(0x1FF                << 0) & 0xFFFFFFFF)
  this.outstanding_req_niso |= (((0x1FF & requestsCount) << 0) & 0xFFFFFFFF)

def setNISOlimitOutstanding(enable):
  if (enable):
    this.outstanding_req_niso |= 1 << 31
  else:
    this.outstanding_req_niso &= (~(1 << 31) & 0xFFFFFFFF)
def setNISOlimitDuringHoldoffOverride(enable):
  if (enable):
    this.outstanding_req_niso |= 1 << 30
  else:
    this.outstanding_req_niso &= (~(1 << 30) & 0xFFFFFFFF)
