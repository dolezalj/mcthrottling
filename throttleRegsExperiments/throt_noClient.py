mcScenarios = {
  'nocli_r0'         : {
     'iter_throt_cycles'                  : [0, 1, 2, 4, 8, 16, 31],
     'apply_iter_throt_to'                : ['r0_high', 'r0_low'],

     'r1_limit_outstanding'               : True,
     'r1_limit_during_holdoff_override'   : False,
     'r1_max_outstanding_requests_d'      : 1,

#     'r0_arb_outstanding_count_above_SMMU': False,
     'clientSets' : [""],
     'handler' : 'mcMultipleThrotCycles',
  },
  'nocli_r0_h'         : {
     'iter_throt_cycles'                  : [0, 1, 2, 4, 8, 16, 31],
     'apply_iter_throt_to'                : ['r0_high'],

     'r1_limit_outstanding'               : True,
     'r1_limit_during_holdoff_override'   : False,
     'r1_max_outstanding_requests_d'      : 1,

#     'r0_arb_outstanding_count_above_SMMU': False,
     'clientSets' : [""],
     'handler' : 'mcMultipleThrotCycles',
  },
  'nocli_r0_l'         : {
     'iter_throt_cycles'                  : [0, 1, 2, 4, 8, 16, 31],
     'apply_iter_throt_to'                : ['r0_low'],

     'r1_limit_outstanding'               : True,
     'r1_limit_during_holdoff_override'   : False,
     'r1_max_outstanding_requests_d'      : 1,

#     'r0_arb_outstanding_count_above_SMMU': False,
     'clientSets' : [""],
     'handler' : 'mcMultipleThrotCycles',
  },
  'nocli_r2'         : {
     'iter_throt_cycles'                  : [0, 1, 2, 4, 8, 16, 31],
     'apply_iter_throt_to'                : ['r3_high', 'r3_low'],

     'r3_limit_outstanding'               : True,
     'r3_limit_during_holdoff_override'   : False,
     'r3_max_outstanding_requests_d'      : 1,

#     'r0_arb_outstanding_count_above_SMMU': False,
     'clientSets' : [""],
     'handler' : 'mcMultipleThrotCycles',
  },
  'nocli_r2_h'         : {
     'iter_throt_cycles'                  : [0, 1, 2, 4, 8, 16, 31],
     'apply_iter_throt_to'                : ['r3_high'],

     'r3_limit_outstanding'               : True,
     'r3_limit_during_holdoff_override'   : False,
     'r3_max_outstanding_requests_d'      : 1,

#     'r0_arb_outstanding_count_above_SMMU': False,
     'clientSets' : [""],
     'handler' : 'mcMultipleThrotCycles',
  },
  'nocli_r2_l'         : {
     'iter_throt_cycles'                  : [0, 1, 2, 4, 8, 16, 31],
     'apply_iter_throt_to'                : ['r3_low'],

     'r3_limit_outstanding'               : True,
     'r3_limit_during_holdoff_override'   : False,
     'r3_max_outstanding_requests_d'      : 1,

#     'r0_arb_outstanding_count_above_SMMU': False,
     'clientSets' : [""],
     'handler' : 'mcMultipleThrotCycles',
  },
  'nocli_niso'       : {
     'iter_throt_cycles'                  : [0, 1, 2, 4, 8, 16, 31],
     'apply_iter_throt_to'                : ['niso_high', 'niso_low'],

     'niso_limit_outstanding'             : True,
     'niso_limit_during_holdoff_override' : False,
     'niso_max_outstanding_requests_d'    : 1,
#     'niso_throttle_disable_cnt_event'    : False,

#     'r0_arb_outstanding_count_above_SMMU': False,
     'clientSets' : [""],
     'handler' : 'mcMultipleThrotCycles',
  },
  'nocli_niso_h'       : {
     'iter_throt_cycles'                  : [0, 1, 2, 4, 8, 16, 31],
     'apply_iter_throt_to'                : ['niso_high'],

     'niso_limit_outstanding'             : True,
     'niso_limit_during_holdoff_override' : False,
     'niso_max_outstanding_requests_d'    : 1,
#     'niso_throttle_disable_cnt_event'    : False,

#     'r0_arb_outstanding_count_above_SMMU': False,
     'clientSets' : [""],
     'handler' : 'mcMultipleThrotCycles',
  },
  'nocli_niso_l'       : {
     'iter_throt_cycles'                  : [0, 1, 2, 4, 8, 16, 31],
     'apply_iter_throt_to'                : ['niso_low'],

     'niso_limit_outstanding'             : True,
     'niso_limit_during_holdoff_override' : False,
     'niso_max_outstanding_requests_d'    : 1,
#     'niso_throttle_disable_cnt_event'    : False,

#     'r0_arb_outstanding_count_above_SMMU': False,
     'clientSets' : [""],
     'handler' : 'mcMultipleThrotCycles',
  },
}


scenarios = {
  'mGPU_iDenver2_nocli_r0'      : {
                              'bw' : 'mPascal_iDenver2',
                              'mc' : 'nocli_r0',
                              'description' : '',
  },
  'mGPU_iDenver2_nocli_r0_h'      : {
                              'bw' : 'mPascal_iDenver2',
                              'mc' : 'nocli_r0_h',
                              'description' : '',
  },
  'mGPU_iDenver2_nocli_r0_l'      : {
                              'bw' : 'mPascal_iDenver2',
                              'mc' : 'nocli_r0_l',
                              'description' : '',
  },
  'mGPU_iDenver2_nocli_r2'      : {
                              'bw' : 'mPascal_iDenver2',
                              'mc' : 'nocli_r2',
                              'description' : '',
  },
  'mGPU_iDenver2_nocli_r2_h'      : {
                              'bw' : 'mPascal_iDenver2',
                              'mc' : 'nocli_r2_h',
                              'description' : '',
  },
  'mGPU_iDenver2_nocli_r2_l'      : {
                              'bw' : 'mPascal_iDenver2',
                              'mc' : 'nocli_r2_l',
                              'description' : '',
  },
  'mGPU_iDenver2_nocli_niso'    : {
                              'bw' : 'mPascal_iDenver2',
                              'mc' : 'nocli_niso',
                              'description' : '',
  },
  'mGPU_iDenver2_nocli_niso_h'    : {
                              'bw' : 'mPascal_iDenver2',
                              'mc' : 'nocli_niso_h',
                              'description' : '',
  },
  'mGPU_iDenver2_nocli_niso_l'    : {
                              'bw' : 'mPascal_iDenver2',
                              'mc' : 'nocli_niso_l',
                              'description' : '',
  },
}
