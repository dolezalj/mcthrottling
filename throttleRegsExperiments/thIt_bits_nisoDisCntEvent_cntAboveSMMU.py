import sys
this = sys.modules[__name__]

baseMCScenarioR0 = {
     'iter_throt_cycles'                  : [0, 1, 2, 4, 8, 16, 31],
     'r1_limit_outstanding'               : True,
     'r1_limit_during_holdoff_override'   : False,
     'r1_max_outstanding_requests_d'      : 1,
     'clientSets' : ["", "GK GK2"],#, "MPCORER", "FTOP", "MPCORER FTOP", "RING1 RING1_OUTPUT"],
     'handler' : 'mcMultipleThrotCycles',
}
baseMCScenarioR0Both = {
     'apply_iter_throt_to'                : ['r0_high', 'r0_low'],
}
baseMCScenarioR0High = {
     'apply_iter_throt_to'                : ['r0_high'],
}
baseMCScenarioR0Low = {
     'apply_iter_throt_to'                : ['r0_low'],
}
baseMCScenarioR2 = {
     'iter_throt_cycles'                  : [0, 1, 2, 4, 8, 16, 31],
     'r3_limit_outstanding'               : True,
     'r3_limit_during_holdoff_override'   : False,
     'r3_max_outstanding_requests_d'      : 1,
     'clientSets' : ["", "GK GK2"],#, "MPCORER", "FTOP", "MPCORER FTOP", "RING1 RING1_OUTPUT"],
     'handler' : 'mcMultipleThrotCycles',
}
baseMCScenarioR2Both = {
     'apply_iter_throt_to'                : ['r3_high', 'r3_low'],
}
baseMCScenarioR2High = {
     'apply_iter_throt_to'                : ['r3_high'],
}
baseMCScenarioR2Low = {
     'apply_iter_throt_to'                : ['r3_low'],
}
baseMCScenarioNISO = {
     'iter_throt_cycles'                  : [0, 1, 2, 4, 8, 16, 31],
     'niso_limit_outstanding'               : True,
     'niso_limit_during_holdoff_override'   : False,
     'niso_max_outstanding_requests_d'      : 1,
     'clientSets' : ["", "GK GK2"],#, "MPCORER", "FTOP", "MPCORER FTOP", "RING1 RING1_OUTPUT"],
     'handler' : 'mcMultipleThrotCycles',
}
baseMCScenarioNISOBoth = {
     'apply_iter_throt_to'                : ['niso_high', 'niso_low'],
}
baseMCScenarioNISOHigh = {
     'apply_iter_throt_to'                : ['niso_high'],
}
baseMCScenarioNISOLow = {
     'apply_iter_throt_to'                : ['niso_low'],
}


# that niso thingbit should anyway influence niso level only
this.mcScenarios = {
  'itTh_r0_both'                         : {
     'r0_arb_outstanding_count_above_SMMU': False,
     'niso_throttle_disable_cnt_event'    : False,
  },
  'itTh_r0_high'                         : {
     'r0_arb_outstanding_count_above_SMMU': False,
     'niso_throttle_disable_cnt_event'    : False,
  },
  'itTh_r0_low'                         : {
     'r0_arb_outstanding_count_above_SMMU': False,
     'niso_throttle_disable_cnt_event'    : False,
  },
  'itTh_r0_both_SMMU'                : {
     'r0_arb_outstanding_count_above_SMMU': True,
     'niso_throttle_disable_cnt_event'    : False,
  },
  'itTh_r0_high_SMMU'                : {
     'r0_arb_outstanding_count_above_SMMU': True,
     'niso_throttle_disable_cnt_event'    : False,
  },
  'itTh_r0_low_SMMU'                 : {
     'r0_arb_outstanding_count_above_SMMU': True,
     'niso_throttle_disable_cnt_event'    : False,
  },
  'itTh_r0_both_nisoDisCntEvt'       : {
     'r0_arb_outstanding_count_above_SMMU': False,
     'niso_throttle_disable_cnt_event'    : True,
  },
  'itTh_r0_high_nisoDisCntEvt'       : {
     'r0_arb_outstanding_count_above_SMMU': False,
     'niso_throttle_disable_cnt_event'    : True,
  },
  'itTh_r0_low_nisoDisCntEvt'        : {
     'r0_arb_outstanding_count_above_SMMU': False,
     'niso_throttle_disable_cnt_event'    : True,
  },
  'itTh_r0_both_SMMU_nisoDisCntEvt'  : {
     'r0_arb_outstanding_count_above_SMMU': True,
     'niso_throttle_disable_cnt_event'    : True,
  },
  'itTh_r0_high_SMMU_nisoDisCntEvt'  : {
     'r0_arb_outstanding_count_above_SMMU': True,
     'niso_throttle_disable_cnt_event'    : True,
  },
  'itTh_r0_low_SMMU_nisoDisCntEvt'   : {
     'r0_arb_outstanding_count_above_SMMU': True,
     'niso_throttle_disable_cnt_event'    : True,
  },

  'itTh_r2_both'                         : {
     'r0_arb_outstanding_count_above_SMMU': False,
     'niso_throttle_disable_cnt_event'    : False,
  },
  'itTh_r2_high'                         : {
     'r0_arb_outstanding_count_above_SMMU': False,
     'niso_throttle_disable_cnt_event'    : False,
  },
  'itTh_r2_low'                         : {
     'r0_arb_outstanding_count_above_SMMU': False,
     'niso_throttle_disable_cnt_event'    : False,
  },
  'itTh_r2_both_SMMU'                : {
     'r0_arb_outstanding_count_above_SMMU': True,
     'niso_throttle_disable_cnt_event'    : False,
  },
  'itTh_r2_high_SMMU'                : {
     'r0_arb_outstanding_count_above_SMMU': True,
     'niso_throttle_disable_cnt_event'    : False,
  },
  'itTh_r2_low_SMMU'                 : {
     'r0_arb_outstanding_count_above_SMMU': True,
     'niso_throttle_disable_cnt_event'    : False,
  },
  'itTh_r2_both_nisoDisCntEvt'       : {
     'r0_arb_outstanding_count_above_SMMU': False,
     'niso_throttle_disable_cnt_event'    : True,
  },
  'itTh_r2_high_nisoDisCntEvt'       : {
     'r0_arb_outstanding_count_above_SMMU': False,
     'niso_throttle_disable_cnt_event'    : True,
  },
  'itTh_r2_low_nisoDisCntEvt'        : {
     'r0_arb_outstanding_count_above_SMMU': False,
     'niso_throttle_disable_cnt_event'    : True,
  },
  'itTh_r2_both_SMMU_nisoDisCntEvt'  : {
     'r0_arb_outstanding_count_above_SMMU': True,
     'niso_throttle_disable_cnt_event'    : True,
  },
  'itTh_r2_high_SMMU_nisoDisCntEvt'  : {
     'r0_arb_outstanding_count_above_SMMU': True,
     'niso_throttle_disable_cnt_event'    : True,
  },
  'itTh_r2_low_SMMU_nisoDisCntEvt'   : {
     'r0_arb_outstanding_count_above_SMMU': True,
     'niso_throttle_disable_cnt_event'    : True,
  },

  'itTh_niso_both'                         : {
     'r0_arb_outstanding_count_above_SMMU': False,
     'niso_throttle_disable_cnt_event'    : False,
  },
  'itTh_niso_high'                         : {
     'r0_arb_outstanding_count_above_SMMU': False,
     'niso_throttle_disable_cnt_event'    : False,
  },
  'itTh_niso_low'                         : {
     'r0_arb_outstanding_count_above_SMMU': False,
     'niso_throttle_disable_cnt_event'    : False,
  },
  'itTh_niso_both_SMMU'                : {
     'r0_arb_outstanding_count_above_SMMU': True,
     'niso_throttle_disable_cnt_event'    : False,
  },
  'itTh_niso_high_SMMU'                : {
     'r0_arb_outstanding_count_above_SMMU': True,
     'niso_throttle_disable_cnt_event'    : False,
  },
  'itTh_niso_low_SMMU'                 : {
     'r0_arb_outstanding_count_above_SMMU': True,
     'niso_throttle_disable_cnt_event'    : False,
  },
  'itTh_niso_both_nisoDisCntEvt'       : {
     'r0_arb_outstanding_count_above_SMMU': False,
     'niso_throttle_disable_cnt_event'    : True,
  },
  'itTh_niso_high_nisoDisCntEvt'       : {
     'r0_arb_outstanding_count_above_SMMU': False,
     'niso_throttle_disable_cnt_event'    : True,
  },
  'itTh_niso_low_nisoDisCntEvt'        : {
     'r0_arb_outstanding_count_above_SMMU': False,
     'niso_throttle_disable_cnt_event'    : True,
  },
  'itTh_niso_both_SMMU_nisoDisCntEvt'  : {
     'r0_arb_outstanding_count_above_SMMU': True,
     'niso_throttle_disable_cnt_event'    : True,
  },
  'itTh_niso_high_SMMU_nisoDisCntEvt'  : {
     'r0_arb_outstanding_count_above_SMMU': True,
     'niso_throttle_disable_cnt_event'    : True,
  },
  'itTh_niso_low_SMMU_nisoDisCntEvt'   : {
     'r0_arb_outstanding_count_above_SMMU': True,
     'niso_throttle_disable_cnt_event'    : True,
  },
}

def init():
  baseMCScenarioR0Both.update(baseMCScenarioR0)
  baseMCScenarioR0High.update(baseMCScenarioR0)
  baseMCScenarioR0Low.update( baseMCScenarioR0)
  baseMCScenarioR2Both.update(baseMCScenarioR2)
  baseMCScenarioR2High.update(baseMCScenarioR2)
  baseMCScenarioR2Low.update( baseMCScenarioR2)
  baseMCScenarioNISOBoth.update(baseMCScenarioNISO)
  baseMCScenarioNISOHigh.update(baseMCScenarioNISO)
  baseMCScenarioNISOLow.update( baseMCScenarioNISO)

  this.mcScenarios['itTh_r0_both'].update(baseMCScenarioR0Both)
  this.mcScenarios['itTh_r0_high'].update(baseMCScenarioR0High)
  this.mcScenarios['itTh_r0_low'].update(baseMCScenarioR0Low)
  this.mcScenarios['itTh_r0_both_SMMU'].update(baseMCScenarioR0Both)
  this.mcScenarios['itTh_r0_high_SMMU'].update(baseMCScenarioR0High)
  this.mcScenarios['itTh_r0_low_SMMU'].update(baseMCScenarioR0Low)
  this.mcScenarios['itTh_r0_both_nisoDisCntEvt'].update(baseMCScenarioR0Both)
  this.mcScenarios['itTh_r0_high_nisoDisCntEvt'].update(baseMCScenarioR0High)
  this.mcScenarios['itTh_r0_low_nisoDisCntEvt'].update(baseMCScenarioR0Low)
  this.mcScenarios['itTh_r0_both_SMMU_nisoDisCntEvt'].update(baseMCScenarioR0Both)
  this.mcScenarios['itTh_r0_high_SMMU_nisoDisCntEvt'].update(baseMCScenarioR0High)
  this.mcScenarios['itTh_r0_low_SMMU_nisoDisCntEvt'].update(baseMCScenarioR0Low)
  
  this.mcScenarios['itTh_r2_both'].update(baseMCScenarioR2Both)
  this.mcScenarios['itTh_r2_high'].update(baseMCScenarioR2High)
  this.mcScenarios['itTh_r2_low'].update(baseMCScenarioR2Low)
  this.mcScenarios['itTh_r2_both_SMMU'].update(baseMCScenarioR2Both)
  this.mcScenarios['itTh_r2_high_SMMU'].update(baseMCScenarioR2High)
  this.mcScenarios['itTh_r2_low_SMMU'].update(baseMCScenarioR2Low)
  this.mcScenarios['itTh_r2_both_nisoDisCntEvt'].update(baseMCScenarioR2Both)
  this.mcScenarios['itTh_r2_high_nisoDisCntEvt'].update(baseMCScenarioR2High)
  this.mcScenarios['itTh_r2_low_nisoDisCntEvt'].update(baseMCScenarioR2Low)
  this.mcScenarios['itTh_r2_both_SMMU_nisoDisCntEvt'].update(baseMCScenarioR2Both)
  this.mcScenarios['itTh_r2_high_SMMU_nisoDisCntEvt'].update(baseMCScenarioR2High)
  this.mcScenarios['itTh_r2_low_SMMU_nisoDisCntEvt'].update(baseMCScenarioR2Low)
  
  this.mcScenarios['itTh_niso_both'].update(baseMCScenarioNISOBoth)
  this.mcScenarios['itTh_niso_high'].update(baseMCScenarioNISOHigh)
  this.mcScenarios['itTh_niso_low'].update(baseMCScenarioNISOLow)
  this.mcScenarios['itTh_niso_both_SMMU'].update(baseMCScenarioNISOBoth)
  this.mcScenarios['itTh_niso_high_SMMU'].update(baseMCScenarioNISOHigh)
  this.mcScenarios['itTh_niso_low_SMMU'].update(baseMCScenarioNISOLow)
  this.mcScenarios['itTh_niso_both_nisoDisCntEvt'].update(baseMCScenarioNISOBoth)
  this.mcScenarios['itTh_niso_high_nisoDisCntEvt'].update(baseMCScenarioNISOHigh)
  this.mcScenarios['itTh_niso_low_nisoDisCntEvt'].update(baseMCScenarioNISOLow)
  this.mcScenarios['itTh_niso_both_SMMU_nisoDisCntEvt'].update(baseMCScenarioNISOBoth)
  this.mcScenarios['itTh_niso_high_SMMU_nisoDisCntEvt'].update(baseMCScenarioNISOHigh)
  this.mcScenarios['itTh_niso_low_SMMU_nisoDisCntEvt'].update(baseMCScenarioNISOLow)

scenarios = {
  'bits_itTh_r0_both'                     : {
    'bw'          : 'mPascal_iDenver2',
    'mc'          : 'itTh_r0_both',
    'description' : '',
  },
  'bits_itTh_r0_high'                     : {
    'bw'          : 'mPascal_iDenver2',
    'mc'          : 'itTh_r0_high',
    'description' : '',
  },
  'bits_itTh_r0_low'                      : {
    'bw'          : 'mPascal_iDenver2',
    'mc'          : 'itTh_r0_low',
    'description' : '',
  },
  'bits_itTh_r0_both_SMMU'                : {
    'bw'          : 'mPascal_iDenver2',
    'mc'          : 'itTh_r0_both_SMMU',
    'description' : '',
  },
  'bits_itTh_r0_high_SMMU'                : {
    'bw'          : 'mPascal_iDenver2',
    'mc'          : 'itTh_r0_high_SMMU',
    'description' : '',
  },
  'bits_itTh_r0_low_SMMU'                 : {
    'bw'          : 'mPascal_iDenver2',
    'mc'          : 'itTh_r0_low_SMMU',
    'description' : '',
  },
  'bits_itTh_r0_both_nisoDisCntEvt'       : {
    'bw'          : 'mPascal_iDenver2',
    'mc'          : 'itTh_r0_both_nisoDisCntEvt',
    'description' : '',
  },
  'bits_itTh_r0_high_nisoDisCntEvt'       : {
    'bw'          : 'mPascal_iDenver2',
    'mc'          : 'itTh_r0_high_nisoDisCntEvt',
    'description' : '',
  },
  'bits_itTh_r0_low_nisoDisCntEvt'        : {
    'bw'          : 'mPascal_iDenver2',
    'mc'          : 'itTh_r0_low_nisoDisCntEvt',
    'description' : '',
  },
  'bits_itTh_r0_both_SMMU_nisoDisCntEvt'  : {
    'bw'          : 'mPascal_iDenver2',
    'mc'          : 'itTh_r0_both_SMMU_nisoDisCntEvt',
    'description' : '',
  },
  'bits_itTh_r0_high_SMMU_nisoDisCntEvt'  : {
    'bw'          : 'mPascal_iDenver2',
    'mc'          : 'itTh_r0_high_SMMU_nisoDisCntEvt',
    'description' : '',
  },
  'bits_itTh_r0_low_SMMU_nisoDisCntEvt'   : {
    'bw'          : 'mPascal_iDenver2',
    'mc'          : 'itTh_r0_low_SMMU_nisoDisCntEvt',
    'description' : '',
  },

  'bits_itTh_r2_both'                     : {
    'bw'          : 'mPascal_iDenver2',
    'mc'          : 'itTh_r2_both',
    'description' : '',
  },
  'bits_itTh_r2_high'                     : {
    'bw'          : 'mPascal_iDenver2',
    'mc'          : 'itTh_r2_high',
    'description' : '',
  },
  'bits_itTh_r2_low'                      : {
    'bw'          : 'mPascal_iDenver2',
    'mc'          : 'itTh_r2_low',
    'description' : '',
  },
  'bits_itTh_r2_both_SMMU'                : {
    'bw'          : 'mPascal_iDenver2',
    'mc'          : 'itTh_r2_both_SMMU',
    'description' : '',
  },
  'bits_itTh_r2_high_SMMU'                : {
    'bw'          : 'mPascal_iDenver2',
    'mc'          : 'itTh_r2_high_SMMU',
    'description' : '',
  },
  'bits_itTh_r2_low_SMMU'                 : {
    'bw'          : 'mPascal_iDenver2',
    'mc'          : 'itTh_r2_low_SMMU',
    'description' : '',
  },
  'bits_itTh_r2_both_nisoDisCntEvt'       : {
    'bw'          : 'mPascal_iDenver2',
    'mc'          : 'itTh_r2_both_nisoDisCntEvt',
    'description' : '',
  },
  'bits_itTh_r2_high_nisoDisCntEvt'       : {
    'bw'          : 'mPascal_iDenver2',
    'mc'          : 'itTh_r2_high_nisoDisCntEvt',
    'description' : '',
  },
  'bits_itTh_r2_low_nisoDisCntEvt'        : {
    'bw'          : 'mPascal_iDenver2',
    'mc'          : 'itTh_r2_low_nisoDisCntEvt',
    'description' : '',
  },
  'bits_itTh_r2_both_SMMU_nisoDisCntEvt'  : {
    'bw'          : 'mPascal_iDenver2',
    'mc'          : 'itTh_r2_both_SMMU_nisoDisCntEvt',
    'description' : '',
  },
  'bits_itTh_r2_high_SMMU_nisoDisCntEvt'  : {
    'bw'          : 'mPascal_iDenver2',
    'mc'          : 'itTh_r2_high_SMMU_nisoDisCntEvt',
    'description' : '',
  },
  'bits_itTh_r2_low_SMMU_nisoDisCntEvt'   : {
    'bw'          : 'mPascal_iDenver2',
    'mc'          : 'itTh_r2_low_SMMU_nisoDisCntEvt',
    'description' : '',
  },

  'bits_itTh_niso_both'                   : {
    'bw'          : 'mPascal_iDenver2',
    'mc'          : 'itTh_niso_both',
    'description' : '',
  },
  'bits_itTh_niso_high'                   : {
    'bw'          : 'mPascal_iDenver2',
    'mc'          : 'itTh_niso_high',
    'description' : '',
  },
  'bits_itTh_niso_low'                    : {
    'bw'          : 'mPascal_iDenver2',
    'mc'          : 'itTh_niso_low',
    'description' : '',
  },
  'bits_itTh_niso_both_SMMU'              : {
    'bw'          : 'mPascal_iDenver2',
    'mc'          : 'itTh_niso_both_SMMU',
    'description' : '',
  },
  'bits_itTh_niso_high_SMMU'              : {
    'bw'          : 'mPascal_iDenver2',
    'mc'          : 'itTh_niso_high_SMMU',
    'description' : '',
  },
  'bits_itTh_niso_low_SMMU'               : {
    'bw'          : 'mPascal_iDenver2',
    'mc'          : 'itTh_niso_low_SMMU',
    'description' : '',
  },
  'bits_itTh_niso_both_nisoDisCntEvt'     : {
    'bw'          : 'mPascal_iDenver2',
    'mc'          : 'itTh_niso_both_nisoDisCntEvt',
    'description' : '',
  },
  'bits_itTh_niso_high_nisoDisCntEvt'     : {
    'bw'          : 'mPascal_iDenver2',
    'mc'          : 'itTh_niso_high_nisoDisCntEvt',
    'description' : '',
  },
  'bits_itTh_niso_low_nisoDisCntEvt'      : {
    'bw'          : 'mPascal_iDenver2',
    'mc'          : 'itTh_niso_low_nisoDisCntEvt',
    'description' : '',
  },
  'bits_itTh_niso_both_SMMU_nisoDisCntEvt': {
    'bw'          : 'mPascal_iDenver2',
    'mc'          : 'itTh_niso_both_SMMU_nisoDisCntEvt',
    'description' : '',
  },
  'bits_itTh_niso_high_SMMU_nisoDisCntEvt': {
    'bw'          : 'mPascal_iDenver2',
    'mc'          : 'itTh_niso_high_SMMU_nisoDisCntEvt',
    'description' : '',
  },
  'bits_itTh_niso_low_SMMU_nisoDisCntEvt' : {
    'bw'          : 'mPascal_iDenver2',
    'mc'          : 'itTh_niso_low_SMMU_nisoDisCntEvt',
    'description' : '',
  },
}
