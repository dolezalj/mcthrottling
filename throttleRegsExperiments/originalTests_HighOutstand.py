mcScenarios = {
  'originalTESTS_r02N_high_outstand'       : {
     'iter_throt_cycles'                  : [0, 1, 2, 4, 8, 16, 31],
     'apply_iter_throt_to'                : ['r0_high', 'r0_low', 'r3_high', 'r3_low', 'niso_high', 'niso_low'],

     'r1_limit_outstanding'               : True,
     'r1_limit_during_holdoff_override'   : False,
     'r1_max_outstanding_requests_d'      : 0x100,

#     'r0_arb_outstanding_count_above_SMMU': False,
     'clientSets' : ["", "GK GK2", "MPCORER", "FTOP", "MPCORER FTOP", "RING1 RING1_OUTPUT"],
     'handler' : 'mcMultipleThrotCycles',
  },
  'originalTESTS_r02_high_outstand'        : {
     'iter_throt_cycles'                  : [0, 1, 2, 4, 8, 16, 31],
     'apply_iter_throt_to'                : ['r0_high', 'r0_low', 'r3_high', 'r3_low'],

     'r1_limit_outstanding'               : True,
     'r1_limit_during_holdoff_override'   : False,
     'r1_max_outstanding_requests_d'      : 0x100,

#     'r0_arb_outstanding_count_above_SMMU': False,
     'clientSets' : ["", "GK GK2", "MPCORER", "FTOP", "MPCORER FTOP", "RING1 RING1_OUTPUT"],
     'handler' : 'mcMultipleThrotCycles',
  },
  'originalTESTS_r0N_high_outstand'        : {
     'iter_throt_cycles'                  : [0, 1, 2, 4, 8, 16, 31],
     'apply_iter_throt_to'                : ['r0_high', 'r0_low', 'niso_high', 'niso_low'],

     'r1_limit_outstanding'               : True,
     'r1_limit_during_holdoff_override'   : False,
     'r1_max_outstanding_requests_d'      : 0x100,

#     'r0_arb_outstanding_count_above_SMMU': False,
     'clientSets' : ["", "GK GK2", "MPCORER", "FTOP", "MPCORER FTOP", "RING1 RING1_OUTPUT"],
     'handler' : 'mcMultipleThrotCycles',
  },
  'originalTESTS_r2N_high_outstand'        : {
     'iter_throt_cycles'                  : [0, 1, 2, 4, 8, 16, 31],
     'apply_iter_throt_to'                : ['r3_high', 'r3_low', 'niso_high', 'niso_low'],

     'r1_limit_outstanding'               : True,
     'r1_limit_during_holdoff_override'   : False,
     'r1_max_outstanding_requests_d'      : 0x100,

#     'r0_arb_outstanding_count_above_SMMU': False,
     'clientSets' : ["", "GK GK2", "MPCORER", "FTOP", "MPCORER FTOP", "RING1 RING1_OUTPUT"],
     'handler' : 'mcMultipleThrotCycles',
  },
  'originalTESTS_r0_high_outstand' : {
     'iter_throt_cycles'                  : [0, 1, 2, 4, 8, 16, 31],
     'apply_iter_throt_to'                : ['r0_high', 'r0_low'],

     'r1_limit_outstanding'               : True,
     'r1_limit_during_holdoff_override'   : False,
     'r1_max_outstanding_requests_d'      : 0x100,

#     'r0_arb_outstanding_count_above_SMMU': False,
     'clientSets' : ["", "GK GK2", "MPCORER", "FTOP", "MPCORER FTOP", "RING1 RING1_OUTPUT"],
     'handler' : 'mcMultipleThrotCycles',
  },
  'originalTESTS_r2_high_outstand' : {
     'iter_throt_cycles'                  : [0, 1, 2, 4, 8, 16, 31],
     'apply_iter_throt_to'                : ['r3_high', 'r3_low'],

     'r3_limit_outstanding'               : True,
     'r3_limit_during_holdoff_override'   : False,
     'r3_max_outstanding_requests_d'      : 0x100,

#     'r0_arb_outstanding_count_above_SMMU': False,
     'clientSets' : ["", "GK GK2", "MPCORER", "FTOP", "MPCORER FTOP", "RING1 RING1_OUTPUT"],
     'handler' : 'mcMultipleThrotCycles',
  },
  'originalTESTS_niso_high_outstand' : {
     'iter_throt_cycles'                  : [0, 1, 2, 4, 8, 16, 31],
     'apply_iter_throt_to'                : ['niso_high', 'niso_low'],

     'niso_limit_outstanding'             : True,
     'niso_limit_during_holdoff_override' : False,
     'niso_max_outstanding_requests_d'    : 0x100,

#     'r0_arb_outstanding_count_above_SMMU': False,
     'clientSets' : ["", "GK GK2", "MPCORER", "FTOP", "MPCORER FTOP", "RING1 RING1_OUTPUT"],
     'handler' : 'mcMultipleThrotCycles',
  },
}

scenarios = {
  'mGPU_iDenver2_r02N_ho'    : {
                              'bw' : 'mPascal_iDenver2',
                              'mc' : 'originalTESTS_r02N_high_outstand',
                              'description' : '',
                            },
  'mGPU_iA57_r02N_ho'        : {
                              'bw' : 'mPascal_iA57',
                              'mc' : 'originalTESTS_r02N_high_outstand',
                              'description' : '',
                            },
  'mGPU_iDenver2_r02_ho'     : {
                              'bw' : 'mPascal_iDenver2',
                              'mc' : 'originalTESTS_r02_high_outstand',
                              'description' : '',
                            },
  'mGPU_iA57_r02_ho'         : {
                              'bw' : 'mPascal_iA57',
                              'mc' : 'originalTESTS_r02_high_outstand',
                              'description' : '',
                            },
  'mGPU_iDenver2_r0N_ho'     : {
                              'bw' : 'mPascal_iDenver2',
                              'mc' : 'originalTESTS_r0N_high_outstand',
                              'description' : '',
                            },
  'mGPU_iA57_r0N_ho'         : {
                              'bw' : 'mPascal_iA57',
                              'mc' : 'originalTESTS_r0N_high_outstand',
                              'description' : '',
                            },
  'mGPU_iDenver2_r2N_ho'     : {
                              'bw' : 'mPascal_iDenver2',
                              'mc' : 'originalTESTS_r2N_high_outstand',
                              'description' : '',
                            },
  'mGPU_iA57_r2N_ho'         : {
                              'bw' : 'mPascal_iA57',
                              'mc' : 'originalTESTS_r2N_high_outstand',
                              'description' : '',
                            },
  'mGPU_iDenver2_r0_ho'      : {
                              'bw' : 'mPascal_iDenver2',
                              'mc' : 'originalTESTS_r0_high_outstand',
                              'description' : '',
                            },
  'mGPU_iA57_r0_ho'          : {
                              'bw' : 'mPascal_iA57',
                              'mc' : 'originalTESTS_r0_high_outstand',
                              'description' : '',
                            },
  'mGPU_iDenver2_r2_ho'      : {
                              'bw' : 'mPascal_iDenver2',
                              'mc' : 'originalTESTS_r2_high_outstand',
                              'description' : '',
                            },
  'mGPU_iA57_r2_ho'          : {
                              'bw' : 'mPascal_iA57',
                              'mc' : 'originalTESTS_r2_high_outstand',
                              'description' : '',
                            },
  'mGPU_iDenver2_niso_ho'    : {
                              'bw' : 'mPascal_iDenver2',
                              'mc' : 'originalTESTS_niso_high_outstand',
                              'description' : '',
                            },
  'mGPU_iA57_niso_ho'        : {
                              'bw' : 'mPascal_iA57',
                              'mc' : 'originalTESTS_niso_high_outstand',
                              'description' : '',
                            },
}
