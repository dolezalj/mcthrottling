mcScenarios = {
  'originalTESTS_r02N'       : {
     'iter_throt_cycles'                  : [0, 1, 2, 4, 8, 16, 31],
     'apply_iter_throt_to'                : ['r0_high', 'r0_low', 'r3_high', 'r3_low', 'niso_high', 'niso_low'],

     'r1_limit_outstanding'               : True,
     'r1_limit_during_holdoff_override'   : False,
     'r1_max_outstanding_requests_d'      : 1,

#     'r0_arb_outstanding_count_above_SMMU': False,
     'clientSets' : ["", "GK GK2", "MPCORER", "FTOP", "MPCORER FTOP", "RING1 RING1_OUTPUT"],
     'handler' : 'mcMultipleThrotCycles',
  },
  'originalTESTS_r02'        : {
     'iter_throt_cycles'                  : [0, 1, 2, 4, 8, 16, 31],
     'apply_iter_throt_to'                : ['r0_high', 'r0_low', 'r3_high', 'r3_low'],

     'r1_limit_outstanding'               : True,
     'r1_limit_during_holdoff_override'   : False,
     'r1_max_outstanding_requests_d'      : 1,

#     'r0_arb_outstanding_count_above_SMMU': False,
     'clientSets' : ["", "GK GK2", "MPCORER", "FTOP", "MPCORER FTOP", "RING1 RING1_OUTPUT"],
     'handler' : 'mcMultipleThrotCycles',
  },
  'originalTESTS_r0N'        : {
     'iter_throt_cycles'                  : [0, 1, 2, 4, 8, 16, 31],
     'apply_iter_throt_to'                : ['r0_high', 'r0_low', 'niso_high', 'niso_low'],

     'r1_limit_outstanding'               : True,
     'r1_limit_during_holdoff_override'   : False,
     'r1_max_outstanding_requests_d'      : 1,

#     'r0_arb_outstanding_count_above_SMMU': False,
     'clientSets' : ["", "GK GK2", "MPCORER", "FTOP", "MPCORER FTOP", "RING1 RING1_OUTPUT"],
     'handler' : 'mcMultipleThrotCycles',
  },
  'originalTESTS_r2N'        : {
     'iter_throt_cycles'                  : [0, 1, 2, 4, 8, 16, 31],
     'apply_iter_throt_to'                : ['r3_high', 'r3_low', 'niso_high', 'niso_low'],

     'r1_limit_outstanding'               : True,
     'r1_limit_during_holdoff_override'   : False,
     'r1_max_outstanding_requests_d'      : 1,

#     'r0_arb_outstanding_count_above_SMMU': False,
     'clientSets' : ["", "GK GK2", "MPCORER", "FTOP", "MPCORER FTOP", "RING1 RING1_OUTPUT"],
     'handler' : 'mcMultipleThrotCycles',
  },
  'originalTESTS_r0'         : {
     'iter_throt_cycles'                  : [0, 1, 2, 4, 8, 16, 31],
     'apply_iter_throt_to'                : ['r0_high', 'r0_low'],

     'r1_limit_outstanding'               : True,
     'r1_limit_during_holdoff_override'   : False,
     'r1_max_outstanding_requests_d'      : 1,

#     'r0_arb_outstanding_count_above_SMMU': False,
     'clientSets' : ["", "GK GK2", "MPCORER", "FTOP", "MPCORER FTOP", "RING1 RING1_OUTPUT"],
     'handler' : 'mcMultipleThrotCycles',
  },
  'originalTESTS_r2'         : {
     'iter_throt_cycles'                  : [0, 1, 2, 4, 8, 16, 31],
     'apply_iter_throt_to'                : ['r3_high', 'r3_low'],

     'r3_limit_outstanding'               : True,
     'r3_limit_during_holdoff_override'   : False,
     'r3_max_outstanding_requests_d'      : 1,

#     'r0_arb_outstanding_count_above_SMMU': False,
     'clientSets' : ["", "GK GK2", "MPCORER", "FTOP", "MPCORER FTOP", "RING1 RING1_OUTPUT"],
     'handler' : 'mcMultipleThrotCycles',
  },
  'originalTESTS_niso'       : {
     'iter_throt_cycles'                  : [0, 1, 2, 4, 8, 16, 31],
     'apply_iter_throt_to'                : ['niso_high', 'niso_low'],

     'niso_limit_outstanding'             : True,
     'niso_limit_during_holdoff_override' : False,
     'niso_max_outstanding_requests_d'    : 1,

#     'r0_arb_outstanding_count_above_SMMU': False,
     'clientSets' : ["", "GK GK2", "MPCORER", "FTOP", "MPCORER FTOP", "RING1 RING1_OUTPUT"],
     'handler' : 'mcMultipleThrotCycles',
  },
}

scenarios = {
  'mGPU_iDenver2_r02N'    : {
                              'bw' : 'mPascal_iDenver2',
                              'mc' : 'originalTESTS_r02N',
                              'description' : '',
                            },
  'mGPU_iA57_r02N'        : {
                              'bw' : 'mPascal_iA57',
                              'mc' : 'originalTESTS_r02N',
                              'description' : '',
                            },
  'mGPU_iDenver2_r02'     : {
                              'bw' : 'mPascal_iDenver2',
                              'mc' : 'originalTESTS_r02',
                              'description' : '',
                            },
  'mGPU_iA57_r02'         : {
                              'bw' : 'mPascal_iA57',
                              'mc' : 'originalTESTS_r02',
                              'description' : '',
                            },
  'mGPU_iDenver2_r0N'     : {
                              'bw' : 'mPascal_iDenver2',
                              'mc' : 'originalTESTS_r0N',
                              'description' : '',
                            },
  'mGPU_iA57_r0N'         : {
                              'bw' : 'mPascal_iA57',
                              'mc' : 'originalTESTS_r0N',
                              'description' : '',
                            },
  'mGPU_iDenver2_r2N'     : {
                              'bw' : 'mPascal_iDenver2',
                              'mc' : 'originalTESTS_r2N',
                              'description' : '',
                            },
  'mGPU_iA57_r2N'         : {
                              'bw' : 'mPascal_iA57',
                              'mc' : 'originalTESTS_r2N',
                              'description' : '',
                            },
  'mGPU_iDenver2_r0'      : {
                              'bw' : 'mPascal_iDenver2',
                              'mc' : 'originalTESTS_r0',
                              'description' : '',
                            },
  'mGPU_iA57_r0'          : {
                              'bw' : 'mPascal_iA57',
                              'mc' : 'originalTESTS_r0',
                              'description' : '',
                            },
  'mGPU_iDenver2_r2'      : {
                              'bw' : 'mPascal_iDenver2',
                              'mc' : 'originalTESTS_r2',
                              'description' : '',
                            },
  'mGPU_iA57_r2'          : {
                              'bw' : 'mPascal_iA57',
                              'mc' : 'originalTESTS_r2',
                              'description' : '',
                            },
  'mGPU_iDenver2_niso'    : {
                              'bw' : 'mPascal_iDenver2',
                              'mc' : 'originalTESTS_niso',
                              'description' : '',
                            },
  'mGPU_iA57_niso'        : {
                              'bw' : 'mPascal_iA57',
                              'mc' : 'originalTESTS_niso',
                              'description' : '',
                            },
}
