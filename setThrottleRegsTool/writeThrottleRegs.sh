#!/bin/bash

SCRIPT_DIR=`dirname $0`
cd ${SCRIPT_DIR}

sudo insmod setThrottleRegs.ko "$@" && dmesg -t | tail -n 20 && sudo rmmod setThrottleRegs 2>/dev/null
