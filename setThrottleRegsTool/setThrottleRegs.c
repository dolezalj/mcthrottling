#include <linux/module.h>	/* Needed by all modules */
#include <linux/kernel.h>	/* Needed for KERN_INFO */
#include <linux/random.h>
#include <linux/smp.h>
#include <linux/interrupt.h>
#include <linux/irqchip/arm-gic.h>
#include <linux/sched.h>   //wake_up_process()
#include <linux/kthread.h> //kthread_create(), kthread_run()
#include <linux/delay.h> //sleep
#include <linux/platform/tegra/mc.h>
/*#include <linux/platform/tegra/mc-regs-t18x.h>*/
#include <linux/platform/tegra/mcerr_ecc_t18x.h>
#include <linux/platform/tegra/tegra18_emc.h>
#include <asm/io.h>
#include <linux/time.h>
#include <linux/delay.h>
#include <linux/highmem.h>
#include <linux/vmalloc.h>

#if !defined(__aarch64__)
	#error Module can only be compiled on ARM 64 machines.
#endif

// differing default values for the throttling before updates to the system
//      mc_writel(0x0,         MC_EMEM_ARB_NISO_THROTTLE_MASK   );
//      mc_writel(0x0,         MC_EMEM_ARB_NISO_THROTTLE_MASK_1 );
//      mc_writel(1<<31|0x40,  MC_EMEM_ARB_OUTSTANDING_REQ      );
//      mc_writel(0x80000080,  MC_EMEM_ARB_OUTSTANDING_REQ_NISO );

#define B(bitNumber, registerValue) ((registerValue >> bitNumber) & 1)

static int readAll = 0;
static int checkWritable = 0;

static u32 ring0_throttle_mask   = 0x80008041;
static u32 niso_throttle_mask    = 0x20400000;
static u32 niso_throttle_mask_1  =    0x40004;
static u32 ring1_throttle        =   0x3f0000;
static u32 ring3_throttle        =        0x0;
static u32 niso_throttle         =   0x3f0000;
static u32 outstanding_req       = 0x80000100;
static u32 outstanding_req_ring3 = 0x80000080;
static u32 outstanding_req_niso  = 0x800000f0;

module_param(readAll, int, 0);
module_param(checkWritable, int, 0);

module_param(ring0_throttle_mask   , uint, 0);
module_param(niso_throttle_mask    , uint, 0);
module_param(niso_throttle_mask_1  , uint, 0);
module_param(ring1_throttle        , uint, 0);
module_param(ring3_throttle        , uint, 0);
module_param(niso_throttle         , uint, 0);
module_param(outstanding_req       , uint, 0);
module_param(outstanding_req_ring3 , uint, 0);
module_param(outstanding_req_niso  , uint, 0);

static uint64_t get_time(struct timespec *time)
{
  return (uint64_t)time->tv_sec * 1000000000 + time->tv_nsec;
}
static uint64_t get_duration(struct timespec *start_time, struct timespec *end_time)
{
  return get_time(end_time) - get_time(start_time);
}

uint64_t readRegs(int, int);
uint64_t readRegs(int header, int checkDiffersFromGlobal) {
  struct timespec time1, time2;

  u32 l_ring0_throttle_mask   ;
  u32 l_niso_throttle_mask    ;
  u32 l_niso_throttle_mask_1  ;
  u32 l_ring1_throttle        ;
  u32 l_ring3_throttle        ;
  u32 l_niso_throttle         ;
  u32 l_outstanding_req       ;
  u32 l_outstanding_req_ring3 ;
  u32 l_outstanding_req_niso  ;

  time1 = current_kernel_time();
  l_ring0_throttle_mask   = mc_readl(MC_EMEM_ARB_RING0_THROTTLE_MASK  );
  l_niso_throttle_mask    = mc_readl(MC_EMEM_ARB_NISO_THROTTLE_MASK   );
  l_niso_throttle_mask_1  = mc_readl(MC_EMEM_ARB_NISO_THROTTLE_MASK_1 );
  l_ring1_throttle        = mc_readl(MC_EMEM_ARB_RING1_THROTTLE       );
  l_ring3_throttle        = mc_readl(MC_EMEM_ARB_RING3_THROTTLE       );
  l_niso_throttle         = mc_readl(MC_EMEM_ARB_NISO_THROTTLE        );
  l_outstanding_req       = mc_readl(MC_EMEM_ARB_OUTSTANDING_REQ      );
  l_outstanding_req_ring3 = mc_readl(MC_EMEM_ARB_OUTSTANDING_REQ_RING3);
  l_outstanding_req_niso  = mc_readl(MC_EMEM_ARB_OUTSTANDING_REQ_NISO );
  time2 = current_kernel_time();

  if (header) printk(KERN_INFO " ### Reading Throttle Registers ### [reading took %lldns]\n", get_duration(&time1, &time2));
  printk(KERN_INFO "%s MC_EMEM_ARB_RING0_THROTTLE_MASK    0x%8x\t[%s%s%s%s%s] ARB_OUTSTANDING_COUNT_ABOVE_SMMU: %s\n"            , (checkDiffersFromGlobal && l_ring0_throttle_mask   != ring0_throttle_mask  ) ? "D" : " ", l_ring0_throttle_mask  , B(15,l_ring0_throttle_mask)?"RING1_OUTPUT ":"", B(7,l_ring0_throttle_mask)?"RING1 ":"", B(6,l_ring0_throttle_mask)?"FTOP ":"", B(2,l_ring0_throttle_mask)?"PTC ":"", B(0,l_ring0_throttle_mask)?"MPCORER ":"", B(31,l_ring0_throttle_mask)?"ENABLE":"DISABLE");
  printk(KERN_INFO "%s MC_EMEM_ARB_NISO_THROTTLE_MASK     0x%8x\t[%s%s%s%s%s%s%s%s%s%s%s%s]\n"                                   , (checkDiffersFromGlobal && l_niso_throttle_mask    != niso_throttle_mask   ) ? "D" : " ", l_niso_throttle_mask   , B(29,l_niso_throttle_mask)?"VICPC ":"", B(28,l_niso_throttle_mask)?"USBD ":"", B(27,l_niso_throttle_mask)?"HOST ":"", B(23,l_niso_throttle_mask)?"SD ":"", B(22,l_niso_throttle_mask)?"GK ":"", B(19,l_niso_throttle_mask)?"MSE ":"", B(17,l_niso_throttle_mask)?"USBX ":"", B(9,l_niso_throttle_mask)?"SAX ":"", B(7,l_niso_throttle_mask)?"PCX ":"", B(3,l_niso_throttle_mask)?"AVP ":"", B(2,l_niso_throttle_mask)?"APB ":"", B(1,l_niso_throttle_mask)?"AHB ":"");
  printk(KERN_INFO "%s MC_EMEM_ARB_NISO_THROTTLE_MASK_1   0x%8x\t[%s%s%s%s%s]\n"                                                 , (checkDiffersFromGlobal && l_niso_throttle_mask_1  != niso_throttle_mask_1 ) ? "D" : " ", l_niso_throttle_mask_1 , B(5,l_niso_throttle_mask_1)?"DFD ":"", B(4,l_niso_throttle_mask_1)?"HDAPC ":"", B(3,l_niso_throttle_mask_1)?"SDM ":"", B(2,l_niso_throttle_mask_1)?"GK2 ":"", B(1,l_niso_throttle_mask_1)?"JPG ":"");
  printk(KERN_INFO "%s MC_EMEM_ARB_RING1_THROTTLE         0x%8x\tth_cyc_high: %4d\tth_cyc_low: %4d\n"                            , (checkDiffersFromGlobal && l_ring1_throttle        != ring1_throttle       ) ? "D" : " ", l_ring1_throttle       , (l_ring1_throttle & 0x003f0000) >> 16       , l_ring1_throttle & 0x3f        );
  printk(KERN_INFO "%s MC_EMEM_ARB_RING3_THROTTLE         0x%8x\tth_cyc_high: %4d\t    th_cyc: %4d\n"                            , (checkDiffersFromGlobal && l_ring3_throttle        != ring3_throttle       ) ? "D" : " ", l_ring3_throttle       , (l_ring3_throttle & 0x001f0000) >> 16       , l_ring3_throttle & 0x1f        );
  printk(KERN_INFO "%s MC_EMEM_ARB_NISO_THROTTLE          0x%8x\tth_cyc_high: %4d\t    th_cyc: %4d\t     disable_cnt_event: %d\n", (checkDiffersFromGlobal && l_niso_throttle         != niso_throttle        ) ? "D" : " ", l_niso_throttle        , (l_niso_throttle & 0x003f0000) >> 16        , l_niso_throttle & 0x3f         , l_niso_throttle >> 31             );
  printk(KERN_INFO "%s MC_EMEM_ARB_OUTSTANDING_REQ        0x%8x\t      count: %4d\t       max: %4d\tlim_en: %d lim_holdoff: %d\n", (checkDiffersFromGlobal && l_outstanding_req       != outstanding_req      ) ? "D" : " ", l_outstanding_req      , (l_outstanding_req & 0x03ff0000) >> 16      , l_outstanding_req & 0x3ff      , l_outstanding_req >> 31           , (l_outstanding_req >> 30) & 1      );
  printk(KERN_INFO "%s MC_EMEM_ARB_OUTSTANDING_REQ_RING3  0x%8x\t      count: %4d\t       max: %4d\tlim_en: %d lim_holdoff: %d\n", (checkDiffersFromGlobal && l_outstanding_req_ring3 != outstanding_req_ring3) ? "D" : " ", l_outstanding_req_ring3, (l_outstanding_req_ring3 & 0x03ff0000) >> 16, l_outstanding_req_ring3 & 0x3ff, l_outstanding_req_ring3 >> 31     , (l_outstanding_req_ring3 >> 30) & 1);
  printk(KERN_INFO "%s MC_EMEM_ARB_OUTSTANDING_REQ_NISO   0x%8x\t                \t       max: %4d\tlim_en: %d lim_holdoff: %d\n", (checkDiffersFromGlobal && l_outstanding_req_niso  != outstanding_req_niso ) ? "D" : " ", l_outstanding_req_niso ,                                               l_outstanding_req_niso & 0x3ff , l_outstanding_req_niso >> 31      , (l_outstanding_req_niso >> 30) & 1 );

  return get_duration(&time1, &time2);
}

int setRegs(void);
int setRegs() {
  struct timespec time1, time2;

  u32 l_ring0_throttle_mask   ;
  u32 l_niso_throttle_mask    ;
  u32 l_niso_throttle_mask_1  ;
  u32 l_ring1_throttle        ;
  u32 l_ring3_throttle        ;
  u32 l_niso_throttle         ;
  u32 l_outstanding_req       ;
  u32 l_outstanding_req_ring3 ;
  u32 l_outstanding_req_niso  ;

  local_irq_disable();

  time1 = current_kernel_time();
  // MC_EMEM_ARB_RING0_THROTTLE_MASK
  //   0 = DISABLE
  //   1 = ENABLE
  // 31 ARB_OUTSTANDING_COUNT_ABOVE_SMMU: If enabled, arb_outstanding count includes requests in SMMU. If disabled, only count requests downstream of ring0.
  //   If enabled, include the client in the ring0 meta-client group for throttling.
  // 15 RING0_THROTTLE_MASK_RING1_OUTPUT
  // 7  RING0_THROTTLE_MASK_RING1
  // 6  RING0_THROTTLE_MASK_FTOP
  // 2  RING0_THROTTLE_MASK_PTC
  // 0  RING0_THROTTLE_MASK_MPCORER
  mc_writel(ring0_throttle_mask, MC_EMEM_ARB_RING0_THROTTLE_MASK);
  // MC_EMEM_ARB_NISO_THROTTLE_MASK_0
  //   0 = DISABLE
  //   1 = ENABLE
  //   If enabled, include the client (below) in the NISO meta-client group for throttling.
  // 29 NISO_THROTTLE_MASK_VICPC
  // 28 NISO_THROTTLE_MASK_USBD
  // 27 NISO_THROTTLE_MASK_HOST
  // 23 NISO_THROTTLE_MASK_SD
  // 22 NISO_THROTTLE_MASK_GK
  // 19 NISO_THROTTLE_MASK_MSE
  // 17 NISO_THROTTLE_MASK_USBX
  // 9  NISO_THROTTLE_MASK_SAX
  // 7  NISO_THROTTLE_MASK_PCX
  // 3  NISO_THROTTLE_MASK_AVP
  // 2  NISO_THROTTLE_MASK_APB
  // 1  NISO_THROTTLE_MASK_AHB
  mc_writel(niso_throttle_mask, MC_EMEM_ARB_NISO_THROTTLE_MASK);
  // MC_EMEM_ARB_NISO_THROTTLE_MASK_1_0
  //   0 = DISABLE
  //   1 = ENABLE
  //   If enabled, include the client (below) in the NISO meta-client group for throttling.
  // 5 NISO_THROTTLE_MASK_DFD
  // 4 NISO_THROTTLE_MASK_HDAPC
  // 3 NISO_THROTTLE_MASK_SDM
  // 2 NISO_THROTTLE_MASK_GK2
  // 1 NISO_THROTTLE_MASK_JPG
  mc_writel(niso_throttle_mask_1, MC_EMEM_ARB_NISO_THROTTLE_MASK_1);
  // MC_EMEM_ARB_RING1_THROTTLE
  // 20:16 RING1_THROTTLE_CYCLES_HIGH: Cycles of throttle after each request when outstanding transaction count is greater than or equal to threshold.
  // 4:0   RING1_THROTTLE_CYCLES_LOW:  Cycles of throttle after each request when outstanding transaction count is below threshold. Suggested programming: (DIV==1)? 1 : 0.
  mc_writel(ring1_throttle,MC_EMEM_ARB_RING1_THROTTLE);
  // MC_EMEM_ARB_RING3_THROTTLE (actually ring2)
  // 4:0   RING3_THROTTLE_CYCLES:      Cycles of throttle after each request.
  mc_writel(ring3_throttle,MC_EMEM_ARB_RING3_THROTTLE);
  // MC_EMEM_ARB_NISO_THROTTLE
  // 31    NISO_THROTTLE_DISABLE_CNT_EVENT: [PMC] Count starts when "above_threshold" and "cnt_event" arrives. If "cnt_event" is disabled,
  //       only "above threshold" is used to start counting. In Parker, Ring2 arbiter's "snap_done" is used as "cnt_event".
  // 21:16 NISO_THROTTLE_CYCLES_HIGH:       [PMC] Cycles of throttle after each request when outstanding transaction count is greater than or equal to threshold.
  // 5:0   NISO_THROTTLE_CYCLES:            [PMC] Cycles of throttle after each request.
  mc_writel(niso_throttle,MC_EMEM_ARB_NISO_THROTTLE);
  // MC_EMEM_ARB_OUTSTANDING_REQ
  // 31    LIMIT_OUTSTANDING:             when ENABLED, the total number of requests in the arbiter is limited to the value in ARB_MAX_OUTSTANDING
  // 30    LIMIT_DURING_HOLDOFF_OVERRIDE: when DISABLED, override the limiting of transactions during hold-off to let requests flow freely
  // 24:16 ARB_OUTSTANDING_COUNT:         Current number of outstanding requests
  // 8:0   ARB_MAX_OUTSTANDING:           The maximum number of requests allowed in the arbiter when LIMIT_OUTSTANDING is set to ENABLE.
  mc_writel(outstanding_req, MC_EMEM_ARB_OUTSTANDING_REQ);
  // MC_EMEM_ARB_OUTSTANDING_REQ_RING3
  // 31    LIMIT_OUTSTANDING_RING3:             When ENABLED, requests into ring3 are throttled after the request count reaches ARB_MAX_OUTSTANDING_RING3.
  // 30    LIMIT_DURING_HOLDOFF_OVERRIDE_RING3: When DISABLED, overrides the limiting of ring3 transactions during holdoff to let requests flow freely.
  // 24:16 ARB_OUTSTANDING_COUNT_RING3:         Current number of outstanding requests
  // 8:0   ARB_MAX_OUTSTANDING_RING3:           The maximum number of requests allowed in the arbiter when LIMIT_OUTSTANDING_RING3 is set to ENABLE
  mc_writel(outstanding_req_ring3, MC_EMEM_ARB_OUTSTANDING_REQ_RING3);
  // MC_EMEM_ARB_OUTSTANDING_REQ_NISO
  // 31    LIMIT_OUTSTANDING_NISO:             When ENABLED, requests into NISO are throttled after the request count reaches ARB_MAX_OUTSTANDING_NISO.
  // 30    LIMIT_DURING_HOLDOFF_OVERRIDE_NISO: When DISABLED, overrides the limiting of NISO transactions during holdoff to let requests flow freely.
  // 24:16 ARB_OUTSTANDING_COUNT_NISO:         Current number of outstanding requests.
  // 8:0   ARB_MAX_OUTSTANDING_NISO:           The maximum number of requests allowed in the arbiter when LIMIT_OUTSTANDING_NISO is set to ENABLE.
  mc_writel(outstanding_req_niso, MC_EMEM_ARB_OUTSTANDING_REQ_NISO);

  do { l_ring0_throttle_mask   = mc_readl(MC_EMEM_ARB_RING0_THROTTLE_MASK  ); } while( l_ring0_throttle_mask                != ring0_throttle_mask   );
  do { l_niso_throttle_mask    = mc_readl(MC_EMEM_ARB_NISO_THROTTLE_MASK   ); } while( l_niso_throttle_mask                 != niso_throttle_mask    );
  do { l_niso_throttle_mask_1  = mc_readl(MC_EMEM_ARB_NISO_THROTTLE_MASK_1 ); } while( l_niso_throttle_mask_1               != niso_throttle_mask_1  );
  do { l_ring1_throttle        = mc_readl(MC_EMEM_ARB_RING1_THROTTLE       ); } while( l_ring1_throttle                     != ring1_throttle        );
  do { l_ring3_throttle        = mc_readl(MC_EMEM_ARB_RING3_THROTTLE       ); } while( l_ring3_throttle                     != ring3_throttle        );
  do { l_niso_throttle         = mc_readl(MC_EMEM_ARB_NISO_THROTTLE        ); } while( l_niso_throttle                      != niso_throttle         );
  do { l_outstanding_req       = mc_readl(MC_EMEM_ARB_OUTSTANDING_REQ      ); } while((l_outstanding_req       & 0xFE00FFFF)!= outstanding_req       );
  do { l_outstanding_req_ring3 = mc_readl(MC_EMEM_ARB_OUTSTANDING_REQ_RING3); } while((l_outstanding_req_ring3 & 0xFE00FFFF)!= outstanding_req_ring3 );
  do { l_outstanding_req_niso  = mc_readl(MC_EMEM_ARB_OUTSTANDING_REQ_NISO ); } while((l_outstanding_req_niso  & 0xFE00FFFF)!= outstanding_req_niso  );

  time2 = current_kernel_time();

  printk(KERN_INFO " ^^^ Writing Throttle Registers ^^^ [writing took %lldns]\n", get_duration(&time1, &time2));

  local_irq_enable();
  return 0;
}

uint64_t checkWritableRegsParts(void);
uint64_t checkWritableRegsParts() {
  ring0_throttle_mask   = 0;
  niso_throttle_mask    = 0;
  niso_throttle_mask_1  = 0;
  ring1_throttle        = 0;
  ring3_throttle        = 0;
  niso_throttle         = 0;
  outstanding_req       = 0;
  outstanding_req_ring3 = 0;
  outstanding_req_niso  = 0;

  printk("*** Writing zeros to all bits of all registers\n");
  setRegs();

  ring0_throttle_mask   = 0xFFFFFFFF;
  niso_throttle_mask    = 0xFFFFFFFF;
  niso_throttle_mask_1  = 0xFFFFFFFF;
  ring1_throttle        = 0xFFFFFFFF;
  ring3_throttle        = 0xFFFFFFFF;
  niso_throttle         = 0xFFFFFFFF;
  outstanding_req       = 0xFFFFFFFF;
  outstanding_req_ring3 = 0xFFFFFFFF;
  outstanding_req_niso  = 0xFFFFFFFF;

  printk("*** Writing ones to all bits of all registers\n");
  mc_writel(ring0_throttle_mask, MC_EMEM_ARB_RING0_THROTTLE_MASK);
  mc_writel(niso_throttle_mask, MC_EMEM_ARB_NISO_THROTTLE_MASK);
  mc_writel(niso_throttle_mask_1, MC_EMEM_ARB_NISO_THROTTLE_MASK_1);
  mc_writel(ring1_throttle,MC_EMEM_ARB_RING1_THROTTLE);
  mc_writel(ring3_throttle,MC_EMEM_ARB_RING3_THROTTLE);
  mc_writel(niso_throttle,MC_EMEM_ARB_NISO_THROTTLE);
  mc_writel(outstanding_req, MC_EMEM_ARB_OUTSTANDING_REQ);
  mc_writel(outstanding_req_ring3, MC_EMEM_ARB_OUTSTANDING_REQ_RING3);
  mc_writel(outstanding_req_niso, MC_EMEM_ARB_OUTSTANDING_REQ_NISO);

  // wait 100ms to be sure the written values settled (setRegs never spotted it would took more or equal to 100ms to set the register values)
  msleep(100);

  readRegs(0, 1);

  return 0;
}

int init_module(void)
{
  if(checkWritable) {
    local_irq_disable();
    checkWritableRegsParts();
    local_irq_enable();
    return 0;
  }
  if(readAll) {
    local_irq_disable();
    readRegs(1, 0);
    local_irq_enable();
    return 0;
  }
  printk(KERN_INFO "\n"
    "ring0_throttle_mask   0x%8x, \n"
    "niso_throttle_mask    0x%8x, \n"
    "niso_throttle_mask_1  0x%8x, \n"
    "ring1_throttle        0x%8x, \n"
    "ring3_throttle        0x%8x, \n"
    "niso_throttle         0x%8x, \n"
    "outstanding_req       0x%8x, \n"
    "outstanding_req_ring3 0x%8x, \n"
    "outstanding_req_niso  0x%8x, \n"
    ,
    ring0_throttle_mask  ,
    niso_throttle_mask   ,
    niso_throttle_mask_1 ,
    ring1_throttle       ,
    ring3_throttle       ,
    niso_throttle        ,
    outstanding_req      ,
    outstanding_req_ring3,
    outstanding_req_niso
  );

  setRegs();
  printk("Check written values by reading them again. [D: differing value]\n");
  readRegs(0, 1);
	return 0;
}

void cleanup_module(void)
{
}

MODULE_LICENSE("GPL");
