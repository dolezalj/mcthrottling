#!/bin/bash

SCRIPT_DIR=`dirname $0`
cd ${SCRIPT_DIR}

sudo insmod setThrottleRegs.ko checkWritable=1 && dmesg -t | tail -n 12 && sudo rmmod setThrottleRegs 2>/dev/null
