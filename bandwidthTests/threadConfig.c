#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include "threadConfig.h"

int checkAllCPUsAreEnabled() {
  cpu_set_t cst;
  pthread_getaffinity_np(pthread_self(), sizeof(cst), &cst);
  for(int i = 0; i < TX2_CPU_NUM; ++i) {
    if(!CPU_ISSET(i, &cst)) {
      return i;
    }
  }
  return -1;
}

void getTX2cpuset(tx2_cpu_affinity_masks_t *cpuMasks) {
  CPU_ZERO(&cpuMasks->all);
  for(int i = 0; i < TX2_CPU_NUM; ++i) {
    CPU_ZERO(&cpuMasks->cpu[i]);
    CPU_SET(i,&cpuMasks->cpu[i]);
    CPU_SET(i,&cpuMasks->all);
  }
  memcpy(&cpuMasks->A57[0], &cpuMasks->cpu[0], sizeof(cpu_set_t));
  memcpy(&cpuMasks->A57[1], &cpuMasks->cpu[3], sizeof(cpu_set_t));
  memcpy(&cpuMasks->A57[2], &cpuMasks->cpu[4], sizeof(cpu_set_t));
  memcpy(&cpuMasks->A57[3], &cpuMasks->cpu[5], sizeof(cpu_set_t));
  memcpy(&cpuMasks->Denver2[0], &cpuMasks->cpu[1], sizeof(cpu_set_t));
  memcpy(&cpuMasks->Denver2[1], &cpuMasks->cpu[2], sizeof(cpu_set_t));
}

