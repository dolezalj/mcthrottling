#ifndef RTAS_EXPERIMENTS_UTILS_H
#define RTAS_EXPERIMENTS_UTILS_H

#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/syscall.h>
#include <errno.h>

static uint64_t get_time()
{
  struct timespec t;
  clock_gettime(CLOCK_MONOTONIC, &t);
  return (uint64_t)t.tv_sec * 1000000000 + t.tv_nsec;
}

/*
 * @param testId 0: all clients
 *               1: throttle GPU
 *               2: throttle GPU+CPU
 *               3: throttle CPU
 *               7: throttle ring1 on ring0
 *               15: set according to passed values
 *               20: clear all throttle related regs
 * @param trotCyclesNumber [THROT0, THROT1, THROT2, THROT4, THROT8, THROT16, THROT31, THROT32, THROT_FULL]
 * @param outstandReqs
 * @param trotRing [THROT_RING_NONE, THROT_RING0, THROT_RING2, THROT_RINGN, THROT_RING02, THROT_RING0N, THROT_RING2N, THROT_RING02N]
 * @param nisomask0 Mask of clients to apply throttling to at NISO level.
 * @param nisomask1 Mask of clients to apply throttling to at NISO level.
 * @param ring0mask Mask of clients to apply throttling to at Ring0 level.
 * @param modparams Buffer of at least 600*sizeof(char) to which modparams are printed for debugging purposes within the caller.
 */
int insmodParams(int testId, int trotCyclesNumber, int outstandReqs, int trotRing, int nisomask0, int nisomask1, int ring0mask, char *modparamsBuffer);

#endif //RTAS_EXPERIMENTS_UTILS_H
