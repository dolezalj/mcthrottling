#ifndef RTAS_EXPERIMENTS_CPUTHREADS_H
#define RTAS_EXPERIMENTS_CPUTHREADS_H

struct elem {
    struct elem *next;
    uint32_t gap[(CACHELINE_SIZE - sizeof(struct elem*))/sizeof(uint32_t)];
};
struct cpu_pointer_chasing_scenario {
    uint64_t size;
    uint64_t steps;
    struct elem* pointerChasingArray;
    float cpu_bandwidth_cumulated;
    int cpu_bandwidth_counts;
};
void *cpu_pointer_chasing_init();
void cpu_pointer_chasing_destroy(void *init_object);
void *cpu_pointer_chasing(void *ptr);

struct cpu_accessedArray_scenario {
    uint64_t size;
    uint64_t steps;
    uint64_t* accessedArray;
    float cpu_bandwidth_cumulated;
    int cpu_bandwidth_counts;
};
void *cpu_accessedArray_init();
void cpu_accessedArray_destroy(void *init_object);
void *cpu_accessedArray(void *ptr);

#endif //RTAS_EXPERIMENTS_CPUTHREADS_H
