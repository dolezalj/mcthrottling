const char *sSDKname = "bwTests";

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <stdint.h>
#include <pthread.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/syscall.h>
#include <string.h>

#include "bwTests.h"
#include "threadConfig.h"
#include "GPUthreads.h"
#include "CPUthreads.h"
#include "utils.h"

pthread_barrier_t cpu_barrier;
pthread_barrier_t sync_cpuANDgpu_barrier;

#define RES_UNDEFINED -2
#define RES_UNKNOWN -1
#define RES_PASCAL  0
#define NAM_PASCAL  "Pascal"
#define RES_A57     1
#define NAM_A57     "A57"
#define RES_DENVER  2
#define NAM_DENVER  "Denver2"


void setCPUscenario(init_function_t *init, destroy_function_t *destroy, thread_function_t *thread, int scenario) {
  switch(scenario) {
    case 1:
      *init = cpu_pointer_chasing_init;
      *destroy = cpu_pointer_chasing_destroy;
      *thread = cpu_pointer_chasing;
      break;
    default:
      *init = cpu_accessedArray_init;
      *destroy = cpu_accessedArray_destroy;
      *thread = cpu_accessedArray;
  }
}

void setGPUscenario(init_function_t *init, destroy_function_t *destroy, thread_function_t *thread, int scenario) {
  switch(scenario) {
    case 1:
      *init = gpu_zero_copy_init;
      *destroy = gpu_zero_copy_destroy;
      *thread = gpu_zero_copy;
    default:
      *init = gpu_device_alloc_init;
      *destroy = gpu_device_alloc_destroy;
      *thread = gpu_device_alloc;
  }
}

int getArgumentResource(char *resourceArg) {
  int resource = RES_UNKNOWN;
  if (!strcmp(resourceArg, NAM_PASCAL)) {
    resource = RES_PASCAL;
  } else if (!strcmp(resourceArg, NAM_A57)) {
    resource = RES_A57;
  } else if (!strcmp(resourceArg, NAM_DENVER)) {
    resource = RES_DENVER;
  }
  return resource;
}

/*
 * Program arguments description: see label 'helpmsg'
 */
int main(int argc, char *argv[]) {
  int status_ret_val = 0;
  int measured_resource = RES_UNDEFINED;
  int interfering_resource = RES_UNDEFINED;
  int cpu_scenario = 1;
  int gpu_scenario = 0;
  int number_of_interf_threads = 0;
  int measure_done = 0;

  int firstDisabledCPU;

  uint32_t measure_thread_repeats = 3;

  init_function_t interfering_init;
  destroy_function_t interfering_destroy;
  thread_function_t interfering_thread;

  init_function_t measure_init;
  destroy_function_t measure_destroy;
  thread_function_t measure_thread;

  for (int i = 1; i < argc; ++i) {
    fprintf(stderr, "%s ", argv[i]);
  }
  fprintf(stderr, "\n");
  /* read parameters */
  int i;
  char errorMsg[100] = "Check the provided input.";
  for (i = 1; i < argc; ++i) {
    /* help message */
    if (!strcmp(argv[i], "-h") || !strcmp(argv[i], "--help"))
      goto helpmsg;
    else
      /* Specify measured resource */
#define MEASURE "measure"
    if (!strncmp(argv[i], MEASURE, strlen(MEASURE))) {
      measured_resource = getArgumentResource(argv[i]+strlen(MEASURE));
      if (measured_resource == RES_UNKNOWN) {
        sprintf(errorMsg, MEASURE " argument not valid");
        goto inperr;
      }
    } else
      /* Specify interfering resource */
#define INTERF "interf"
    if (!strncmp(argv[i], INTERF, strlen(INTERF))) {
      interfering_resource = getArgumentResource(argv[i]+strlen(INTERF));
      if (interfering_resource == RES_UNKNOWN) {
        sprintf(errorMsg, INTERF " argument not valid");
        goto inperr;
      }
      if (number_of_interf_threads == 0) number_of_interf_threads = 1;
    } else
      /* Read # of interfering threads. */
#define INTERFTH "threads"
    if (!strncmp(argv[i], INTERFTH, strlen(INTERFTH))) {
      number_of_interf_threads = atoi(argv[i] + strlen(INTERFTH));
      if (number_of_interf_threads > 3) {
        sprintf(errorMsg, INTERFTH " argument integer must be in range 0-3");
        goto inperr;
      }
    } else
      /* Read # of interfering threads. */
#define MEASRPTS "measRpts"
    if (!strncmp(argv[i], MEASRPTS, strlen(MEASRPTS))) {
      measure_thread_repeats = atoi(argv[i] + strlen(MEASRPTS));
      if (measure_thread_repeats < 0) {
        sprintf(errorMsg, MEASRPTS " argument integer must be positive");
        goto inperr;
      }
    }
      /* unsupported option */
    else {
      sprintf(errorMsg, "Provided argument `%s' is unknown.", argv[i]);
      goto inperr;
    }
  }
  if (measured_resource < 0) {
    sprintf(errorMsg, "You must specify one resource to be measured.");
    goto inperr;
  }
  if (measured_resource == interfering_resource) {
    sprintf(errorMsg, "It is not implemented to have the same measured and interfering resource.");
    goto inperr;
  }
  switch (interfering_resource) {
    case RES_DENVER:
      if (number_of_interf_threads > 2) {
        sprintf(errorMsg, "Denver can interfere utilizing at most 2 threads.");
        goto inperr;
      }
      break;
    case RES_PASCAL:
      if (number_of_interf_threads > 1) {
        sprintf(errorMsg, "Pascal can interfere utilizing at most 1 thread.");
        goto inperr;
      }
      break;
  }

  firstDisabledCPU = checkAllCPUsAreEnabled();
  if (firstDisabledCPU != -1) {
    fprintf(stderr, "Enable all CPUs, including CPU %d!\n", firstDisabledCPU);
  }

  tx2_cpu_affinity_masks_t TX2CPUmasks;
  getTX2cpuset(&TX2CPUmasks);
  cpu_set_t *cpu_pool_masks;
  if (measured_resource == RES_DENVER) {
    cpu_pool_masks = TX2CPUmasks.Denver2;
  } else {
    cpu_pool_masks = TX2CPUmasks.A57;
  }

  // set memory controller
  thread_conf_t config_measure_thread;
  thread_conf_t config_interfering_threads[3];

  for (int th_it = 0; th_it < number_of_interf_threads; ++th_it) {
    switch(interfering_resource) {
      case RES_A57:
        setCPUscenario(&interfering_init, &interfering_destroy, &interfering_thread, cpu_scenario);
        config_interfering_threads[th_it].scenario_struct = interfering_init();
        config_interfering_threads[th_it].repeats = -1ul;
        config_interfering_threads[th_it].res_nam = NAM_A57;
        config_interfering_threads[th_it].thread_index = th_it;
        config_interfering_threads[th_it].measure_done = &measure_done;
        config_interfering_threads[th_it].th_affinity = &TX2CPUmasks.A57[th_it];
        pthread_create(&config_interfering_threads[th_it].thread, NULL, interfering_thread, (void *)&config_interfering_threads[th_it]);
        break;
      case RES_DENVER:
        setCPUscenario(&interfering_init, &interfering_destroy, &interfering_thread, cpu_scenario);
        config_interfering_threads[th_it].scenario_struct = interfering_init();
        config_interfering_threads[th_it].repeats = -1ul;
        config_interfering_threads[th_it].res_nam = NAM_DENVER;
        config_interfering_threads[th_it].thread_index = th_it;
        config_interfering_threads[th_it].measure_done = &measure_done;
        config_interfering_threads[th_it].th_affinity = &TX2CPUmasks.Denver2[th_it];
        pthread_create(&config_interfering_threads[th_it].thread, NULL, interfering_thread, (void *)&config_interfering_threads[th_it]);
        break;
      case RES_PASCAL:
        setGPUscenario(&interfering_init, &interfering_destroy, &interfering_thread, gpu_scenario);
        config_interfering_threads[th_it].scenario_struct = interfering_init();
        config_interfering_threads[th_it].repeats = -1ul;
        config_interfering_threads[th_it].res_nam = NAM_PASCAL;
        config_interfering_threads[th_it].thread_index = 3;
        config_interfering_threads[th_it].measure_done = &measure_done;
        config_interfering_threads[th_it].th_affinity = &TX2CPUmasks.A57[3];
        pthread_create(&config_interfering_threads[th_it].thread, NULL, interfering_thread, (void *)&config_interfering_threads[th_it]);
        break;
    }
  }
  switch(measured_resource) {
    case RES_A57:
      setCPUscenario(&measure_init, &measure_destroy, &measure_thread, cpu_scenario);
      config_measure_thread.scenario_struct = measure_init();
      config_measure_thread.repeats = measure_thread_repeats;
      config_measure_thread.res_nam = NAM_A57;
      config_measure_thread.thread_index = 3;
      config_measure_thread.measure_done = &measure_done;
      config_measure_thread.th_affinity = &TX2CPUmasks.A57[3];
      pthread_create(&config_measure_thread.thread, NULL, measure_thread, (void *)&config_measure_thread);
      pthread_join(config_measure_thread.thread, NULL);
      measure_destroy(config_measure_thread.scenario_struct);
      break;
    case RES_DENVER:
      setCPUscenario(&measure_init, &measure_destroy, &measure_thread, cpu_scenario);
      config_measure_thread.scenario_struct = measure_init();
      config_measure_thread.repeats = measure_thread_repeats;
      config_measure_thread.res_nam = NAM_DENVER;
      config_measure_thread.thread_index = 1;
      config_measure_thread.measure_done = &measure_done;
      config_measure_thread.th_affinity = &TX2CPUmasks.Denver2[1];
      pthread_create(&config_measure_thread.thread, NULL, measure_thread, (void *)&config_measure_thread);
      pthread_join(config_measure_thread.thread, NULL);
      measure_destroy(config_measure_thread.scenario_struct);
      break;
    case RES_PASCAL:
      setGPUscenario(&measure_init, &measure_destroy, &measure_thread, gpu_scenario);
      config_measure_thread.scenario_struct = measure_init();
      config_measure_thread.repeats = measure_thread_repeats;
      config_measure_thread.res_nam = NAM_PASCAL;
      config_measure_thread.thread_index = 3;
      config_measure_thread.measure_done = &measure_done;
      config_measure_thread.th_affinity = &TX2CPUmasks.A57[3];
      pthread_create(&config_measure_thread.thread, NULL, measure_thread, (void *)&config_measure_thread);
      pthread_join(config_measure_thread.thread, NULL);
      measure_destroy(config_measure_thread.scenario_struct);
      break;
  }
  measure_done = 1;
  for (int th_it = 0; th_it < number_of_interf_threads; ++th_it) {
    pthread_join(config_interfering_threads[th_it].thread, NULL);
    interfering_destroy(config_interfering_threads[th_it].scenario_struct);
  }

//  pthread_barrier_init(&cpu_barrier, NULL, number_of_cpu_threads);
//  pthread_barrier_init(&sync_cpuANDgpu_barrier, NULL, number_of_all_threads);

  /* print results */
  printf("M %f ", config_measure_thread.memory_bandwidth);
  for (int th_it = 0; th_it < number_of_interf_threads; ++th_it) {
    printf("I%d %f ", th_it, config_interfering_threads[th_it].memory_bandwidth);
  }
  printf("\n");
  return status_ret_val;

inperr:
  fprintf(stderr, "Error! %s\n", errorMsg);
  status_ret_val = 1;
helpmsg:
  fprintf(stderr,
          "Measure memory bandwidth of TX2 resources (GPU " NAM_PASCAL ", CPU " NAM_A57 " core within 4core cluster, CPU " NAM_DENVER " core within 2core cluster). The tool allows to interfere (run memory intensive tasks) from other sources and also to throttle bandwidth of measured resource on the memory controller level.\n"
          "Usage: %s measure{" NAM_PASCAL ";" NAM_A57 ";" NAM_DENVER "} [interf{" NAM_PASCAL ";" NAM_A57 ";" NAM_DENVER "}] [threads{<1-3>}] [THROT{<0-63>}] [CLIENT{gk;mpcorer;ftop;ring1}]* [RING{0;2;N;02;0N;2N;02N}]*\n"
          "  measure{x} \t.. Measure memory bandwidth of resource {x} running memory intensive thread on that resource.\n"
          "  interf{y}  \t.. Introduce measurement interference from resource {y} by running memory intensive thread on that resource.\n"
          "             \t.. If specified imposes one thread unless threads{z} says differently.\n"
          "  threads{z} \t.. Number of interfering memory bandwidth intensive threads to be started.\n"
          "             \t   Valid values: <1-3> for A57; <1-2> for Denver2\n"
          "  measRpts{w}\t.. Set number of repeats of measure thread (average bandwidth will be reported).\n"
          "\n"
          , argv[0]);
  return status_ret_val;
}
