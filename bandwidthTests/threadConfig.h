#ifndef RTAS_EXPERIMENTS_THREADCONFIG_H
#define RTAS_EXPERIMENTS_THREADCONFIG_H

#include <pthread.h>
#include <stdint.h>

#include "platformConfig.h"

typedef struct {
  cpu_set_t cpu[TX2_CPU_NUM];
  cpu_set_t all;
  cpu_set_t A57[TX2_A57_CPU_NUM];
  cpu_set_t Denver2[TX2_DENVER2_CPU_NUM];
} tx2_cpu_affinity_masks_t;

/**
 * Initialize CPU sets (to run threads on dedicated CPU)
 * Get cpu_set_t for single A57 and single Denver2 cores. Get also cpu_set_t for all cores.
 * @param cpuMasks pointer to the place where this function stores the masks of different cpu combinations
 */
void getTX2cpuset(tx2_cpu_affinity_masks_t *cpuMasks);

/**
 * @returns -1 when all are enabled
 *          i number of the first disabled CPU
 */
int checkAllCPUsAreEnabled(void);


typedef void *(*init_function_t)(void);
typedef void(*destroy_function_t)(void *);
typedef void *(*thread_function_t)(void *);


struct thread_conf;


typedef struct thread_conf {
    pthread_t thread;
    cpu_set_t *th_affinity;
    void *scenario_struct;
    uint64_t repeats;
    int *measure_done;
    const char *res_nam;

    uint64_t thread_index;
    float memory_bandwidth;
} thread_conf_t;

#endif //RTAS_EXPERIMENTS_THREADCONFIG_H
