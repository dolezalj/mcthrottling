#include <stdlib.h>
#include <stdio.h>

#include "threadConfig.h"
#include "CPUthreads.h"
#include "utils.h"
#include "platformConfig.h"


struct elem* makeArray(int length, bool sequential) {
  int i;
  struct elem *array = (struct elem *) calloc(length, sizeof(struct elem));
  struct elem *p;

  if (sequential) {
    for (i = 0; i < length - 1; i++) array[i].next = &array[i + 1];
    array[length - 1].next = &array[0];
  } else {
    for (i = 0; i < length; ++i) {
      array[i].next = &array[(rand() % length)];
    }
    uint8_t* visited = (uint8_t*)calloc(length, sizeof(int));
    struct elem* currentLoopStart;
    for (i = 0; i < length; ++i) {
      if (!visited[i]) {
        currentLoopStart = &array[i];
        p = currentLoopStart;
        visited[p - &array[0]] = 1;
        while (!visited[p->next - &array[0]]) {
          p = p->next;
          visited[p - &array[0]] = 1;
        }
        p->next = currentLoopStart;
        p = currentLoopStart->next;
        currentLoopStart->next = array[0].next;
        array[0].next = p;
      }
    }
    free(visited);
  }
  return array;
}
void *cpu_pointer_chasing_init() {
  struct cpu_pointer_chasing_scenario *pcs = (struct cpu_pointer_chasing_scenario *)malloc(sizeof(struct cpu_pointer_chasing_scenario));
  pcs->size = 20 * (1 << 20);
  pcs->steps = 10 * pcs->size;
  pcs->pointerChasingArray = makeArray(pcs->size, 1);
  return pcs;
}
void cpu_pointer_chasing_destroy(void *init_object) {
  struct cpu_pointer_chasing_scenario * pcs = (struct cpu_pointer_chasing_scenario *)init_object;
  free(pcs->pointerChasingArray);
  free(pcs);
}
void *cpu_pointer_chasing(void *ptr) {
  thread_conf_t *conf = (thread_conf_t *)ptr;
  struct cpu_pointer_chasing_scenario * pcs = (struct cpu_pointer_chasing_scenario *)conf->scenario_struct;

  uint64_t count,time,time1,time2;
  uint64_t i = conf->thread_index;

  uint32_t cycle;
  pthread_setaffinity_np(pthread_self(), sizeof(cpu_set_t), conf->th_affinity);

  struct elem *p = pcs->pointerChasingArray;
  for (cycle = 0; cycle < conf->repeats && !*conf->measure_done; cycle++)
  {
    time1 = get_time();
    count = pcs->steps;
    while (--count){
      p->gap[0]++;
      p=p->next;
    }
    time2 = get_time();

    time = time2-time1;
    float bw = (pcs->steps*sizeof(struct elem))/(time*1.0f);
    fprintf(stderr, "%s[%ld]:,%ld, ,[ms], bw: %f GB/s\n",conf->res_nam,i,time/1000000,bw);
    pcs->cpu_bandwidth_cumulated += bw;
    pcs->cpu_bandwidth_counts += 1;
  }
  conf->memory_bandwidth = pcs->cpu_bandwidth_cumulated/pcs->cpu_bandwidth_counts;

  return NULL;
}




void *cpu_accessedArray_init() {
  struct cpu_accessedArray_scenario *aas = (struct cpu_accessedArray_scenario *)malloc(sizeof(struct cpu_accessedArray_scenario));
  aas->size = 200 * (1 << 20);
  aas->steps = aas->size;
  aas->accessedArray = (uint64_t *) malloc(aas->size * sizeof(uint64_t));
  return aas;
}
void cpu_accessedArray_destroy(void *init_object) {
  struct cpu_accessedArray_scenario * aas = (struct cpu_accessedArray_scenario *)init_object;
  free(aas->accessedArray);
  free(aas);
}
void *cpu_accessedArray(void *ptr) {
  thread_conf_t *conf = (thread_conf_t *)ptr;
  struct cpu_accessedArray_scenario * aas = (struct cpu_accessedArray_scenario *)conf->scenario_struct;

  uint64_t count,time,time1,time2;
  uint64_t i = conf->thread_index;

  uint32_t cycle;
  pthread_setaffinity_np(pthread_self(), sizeof(cpu_set_t), conf->th_affinity);

  uint64_t* p = aas->accessedArray;
  volatile register uint64_t readVal;
  for (cycle = 0; cycle < conf->repeats && !*conf->measure_done; cycle++)
  {
    time1 = get_time();
    count = 0;
    while (count < aas->steps) {
      //p[(count*(CACHELINE_SIZE/sizeof(uint64_t))) % aas->size] = 2*count;           // WRITES
      readVal = p[(count*(CACHELINE_SIZE/sizeof(uint64_t))) % aas->size];           // READS
      count++;
    }
    time2 = get_time();
    time = time2-time1;
    float bw = (1.0f*aas->steps*CACHELINE_SIZE)/(time*1.0f);
    fprintf(stderr, "%s[%ld]:,%lf, ,[ms], bw: %f GB/s\n",conf->res_nam,i,time/1000000.0f,bw);
    aas->cpu_bandwidth_cumulated += bw;
    aas->cpu_bandwidth_counts += 1;
  }
  conf->memory_bandwidth = aas->cpu_bandwidth_cumulated/aas->cpu_bandwidth_counts;

  return NULL;
}
