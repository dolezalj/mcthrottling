#include <stdlib.h>
#include <stdio.h>

#include "bwTests.h"
#include "utils.h"

#define delete_module(name, flags) syscall(__NR_delete_module, name, flags)
static inline int finit_module(int fd, const char *uargs, int flags)
{
  return syscall(__NR_finit_module, fd, uargs, flags);
}

int insmod(const char* param){
  long res;
  int fd;

  fd = open("../throttle.ko", O_RDONLY | O_CLOEXEC);
  if (fd < 0) {
    perror("Unable to open module file");
    return EXIT_FAILURE;
  }
  res = finit_module(fd, param, 0);
  if (res != 0) {
    perror("Error when loading module");
    close(fd);
    return EXIT_FAILURE;
  }
  close(fd);
  if (delete_module("throttle", O_NONBLOCK) != 0) {
    perror("delete_module");
    return EXIT_FAILURE;
  }
  return 0;
}

int insmodParams(int testId, int trotCyclesNumber, int outstandReqs, int trotRing, int nisomask0, int nisomask1, int ring0mask, char *modparamsBuffer) {
  char modparams[200];
  sprintf(modparams, "testId=%d trot=0x%x outstand=%d trotring=0x%x nisomask0=0x%x nisomask1=0x%x ring0mask=0x%x",
          testId, trotCyclesNumber, outstandReqs, trotRing, nisomask0, nisomask1, ring0mask);
  if (modparamsBuffer != NULL) {
    sprintf(modparamsBuffer, "%s [rings: %s%s%s]", modparams,
            trotRing & THROT_RING0 ? "0" : "", trotRing & THROT_RING2 ? "2" : "", trotRing & THROT_RINGN ? "N" : "");
  }
  return insmod(modparams);
}
