#ifndef RTAS_EXPERIMENTS_GPUTHREADS_H
#define RTAS_EXPERIMENTS_GPUTHREADS_H

#include <stdint.h>

struct gpu_device_alloc_scenario {
    uint64_t size;
    float *x;
    float *y;
    float *z;
    float gpu_bandwidth_cumulated;
    int gpu_bandwidth_counts;
};
void *gpu_device_alloc_init();
void gpu_device_alloc_destroy(void *init_object);
void *gpu_device_alloc(void *conf);

struct gpu_zero_copy_scenario {
    uint64_t size;
    float *x;
    float *y;
    float *z;
    float gpu_bandwidth_cumulated;
    int gpu_bandwidth_counts;
};
void *gpu_zero_copy_init();
void gpu_zero_copy_destroy(void *init_object);
void *gpu_zero_copy(void *conf);

#endif //RTAS_EXPERIMENTS_GPUTHREADS_H
