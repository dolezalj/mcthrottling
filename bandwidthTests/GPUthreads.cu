#include <stdlib.h>
#include <stdio.h>
#include <cuda_runtime.h>

#include "threadConfig.h"
#include "GPUthreads.h"
#include "utils.h"

// ### GPU reusable kernels
__global__
void saxpy(int n, float a, float *x, float *y, float *z)
{
  int i = blockIdx.x*blockDim.x + threadIdx.x;
  if (i < n) z[i] = a*x[i] + y[i];
}

// ### GPU scenarios
void *gpu_device_alloc_init() {
  struct gpu_device_alloc_scenario *das = (struct gpu_device_alloc_scenario *)malloc(sizeof(struct gpu_device_alloc_scenario));
  das->size = 200 * (1 << 20);
  das->x = (float *)malloc(das->size * sizeof(float));
  das->y = (float *)malloc(das->size * sizeof(float));
  das->z = (float *)malloc(das->size * sizeof(float));
  for (int i = 0; i < das->size; i++) {
    das->x[i] = 1.0f;
    das->y[i] = 2.0f;
    das->z[i] = 0.0f;
  }
  return (void *)das;
}
void gpu_device_alloc_destroy(void *init_object) {
  struct gpu_device_alloc_scenario *das = (struct gpu_device_alloc_scenario *)init_object;
  free(das->x);
  free(das->y);
  free(das->z);
  free(das);
}
void *gpu_device_alloc(void *ptr) {
  thread_conf_t *conf = (thread_conf_t *)ptr;
  struct gpu_device_alloc_scenario *das = (struct gpu_device_alloc_scenario *)conf->scenario_struct;
  float *d_x, *d_y;
  float miliseconds = 0;
  long timeB,timeE;

  pthread_setaffinity_np(pthread_self(), sizeof(cpu_set_t), conf->th_affinity);
  uint64_t N = das->size;

  cudaMalloc(&d_x, N*sizeof(float));
  cudaMalloc(&d_y, N*sizeof(float));
  cudaMemcpy(d_x, das->x, N*sizeof(float), cudaMemcpyHostToDevice);
  cudaMemcpy(d_y, das->y, N*sizeof(float), cudaMemcpyHostToDevice);

  cudaEvent_t start, stop;
  cudaEventCreateWithFlags(&start,cudaEventBlockingSync);
  cudaEventCreateWithFlags(&stop,cudaEventBlockingSync);

  for (uint64_t i = 0; i < conf->repeats && !*conf->measure_done; ++i) {
    timeB = get_time();
    cudaEventRecord(start);

    saxpy << < (N + 255) / 256, 256 >> > (N, 2.0f, d_x, d_y, d_y);
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    timeE = get_time();
    cudaMemcpy(das->z, d_y, N * sizeof(float), cudaMemcpyDeviceToHost);
#ifdef CHECK_COMPUTATIONS_YIELD_EXPECTED_RESULTS
    float maxError = 0.0f;
    for(int i = 0; i < N; ++i) {
      maxError = max(maxError, abs(das->z[i]-4.0f));
    }
    if(maxError != 0.0f) {
      fprintf(stderr, "Kernel computation error: %f\n", maxError);
    }
#endif
    cudaEventElapsedTime(&miliseconds, start, stop);
    float bw = (N * sizeof(float) * 3.0f) / (timeE - timeB);
    fprintf(stderr, "%s, , %ld, %lf, bw: %f GB/s\n",conf->res_nam, (timeE - timeB) / 1000000, miliseconds, bw);
    das->gpu_bandwidth_cumulated += bw;
    das->gpu_bandwidth_counts += 1;
  }
  conf->memory_bandwidth = das->gpu_bandwidth_cumulated/das->gpu_bandwidth_counts;
  cudaFree(d_x);
  cudaFree(d_y);
  return NULL;
}


void *gpu_zero_copy_init() {
  struct gpu_zero_copy_scenario *zcs = (struct gpu_zero_copy_scenario *)malloc(sizeof(struct gpu_zero_copy_scenario));
  zcs->size = 200 * (1 << 20);
  cudaHostAlloc((void **)&zcs->x, zcs->size*sizeof(float), cudaHostAllocMapped);
  cudaHostAlloc((void **)&zcs->y, zcs->size*sizeof(float), cudaHostAllocMapped);
  cudaHostAlloc((void **)&zcs->z, zcs->size*sizeof(float), cudaHostAllocMapped);
  for (int i = 0; i < zcs->size; i++) {
    zcs->x[i] = 1.0f;
    zcs->y[i] = 2.0f;
    zcs->z[i] = 0.0f;
  }
  return (void *)zcs;
}
void gpu_zero_copy_destroy(void *init_object) {
  struct gpu_zero_copy_scenario *zcs = (struct gpu_zero_copy_scenario *)init_object;
  cudaFreeHost(zcs->x);
  cudaFreeHost(zcs->y);
  cudaFreeHost(zcs->z);
}
void *gpu_zero_copy(void *ptr) {
  thread_conf_t *conf = (thread_conf_t *)ptr;
  struct gpu_zero_copy_scenario *zcs = (struct gpu_zero_copy_scenario *)conf->scenario_struct;
  float *d_x, *d_y, *d_z;
  float miliseconds = 0;
  long timeB,timeE;

  pthread_setaffinity_np(pthread_self(), sizeof(cpu_set_t), conf->th_affinity);
  uint64_t N = zcs->size;

  cudaHostGetDevicePointer((void **) &d_x, (void *) zcs->x, 0);
  cudaHostGetDevicePointer((void **) &d_y, (void *) zcs->y, 0);
  cudaHostGetDevicePointer((void **) &d_z, (void *) zcs->z, 0);

  cudaEvent_t start, stop;
  cudaEventCreateWithFlags(&start,cudaEventBlockingSync);
  cudaEventCreateWithFlags(&stop,cudaEventBlockingSync);

  for (uint64_t i = 0; i < conf->repeats && !*conf->measure_done; ++i) {
    timeB = get_time();
    cudaEventRecord(start);
    saxpy << < (N + 255) / 256, 256 >> > (N, 2.0f, d_x, d_y, d_z);

    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    timeE = get_time();
#ifdef CHECK_COMPUTATIONS_YIELD_EXPECTED_RESULTS
    float maxError = 0.0f;
    for(int i = 0; i < N; ++i) {
      maxError = max(maxError, abs(conf->dst_buffer[i]-4.0f));
    }
    if(maxError != 0.0f) {
      fprintf(stderr, "Kernel computation error: %f\n", maxError);
    }
#endif
    cudaEventElapsedTime(&miliseconds, start, stop);
    float bw = (N * sizeof(float) * 3.0f) / (timeE - timeB);
    fprintf(stderr, "%s, , %ld, %lf, bw: %f GB/s\n",conf->res_nam, (timeE - timeB) / 1000000, miliseconds, bw);
    zcs->gpu_bandwidth_cumulated += bw;
    zcs->gpu_bandwidth_counts += 1;
  }
  conf->memory_bandwidth = zcs->gpu_bandwidth_cumulated/zcs->gpu_bandwidth_counts;
  return NULL;
}
