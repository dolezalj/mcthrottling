#!/usr/bin/python

import matplotlib
import matplotlib.pyplot as plt
from matplotlib import ticker
import numpy as np
import sys
import os

def ticks_format(value, index):
    """
    get the value and returns the value as:
       integer: [0,99]
       1 digit float: [0.1, 0.99]
       n*10^m: otherwise
    To have all the number of the same size they are all returned as latex strings
    """
    exp = np.floor(np.log10(value))
    base = value/10**exp
    if exp == 0 or exp == 1:
        return '${0:d}$'.format(int(value))
    if exp == -1:
        return '${0:.1f}$'.format(value)
    else:
        return '${0:d}\\times10^{{{1:d}}}$'.format(int(base), int(exp))

experimentfilename=sys.argv[1]
testname=os.path.splitext(experimentfilename)[0]
caption=testname.replace('_', ' ')

measurementName = ""
x = []
y = []
l = []

yIsLogScale=False
xIsLogScale=True

fig = plt.figure()
ax = fig.add_subplot(111)
plt.xlabel('# of throttle cycles')
plt.ylabel('Bandwidth [GB/s]')
plt.title(caption)
#plt.yticks(np.arange(0, 50, 2))

markers=['.', '+', 'x']
colors=['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2']
nextColor=0
nextMarker=0

with open(experimentfilename, 'r') as expfile:
  for line in expfile:
    if(line.find('##') != -1):
      line = line[0:line.find('##')] # remove comments i.e. text after and including double hash `##'
    if(len(line.strip()) == 0): # ignore empty lines
      continue
    if(line[0] == '#'):
      measurementName=line[2:-1]
      i = 0
      while i < len(y):
        plt.plot(x, y[i], label=l[i], marker=markers[nextMarker], color=colors[nextColor])
        nextMarker = (nextMarker+1)%len(markers)
        i+=1
      nextColor = (nextColor+1)%len(colors)
      x = []
      y = []
      l = []
      continue
    tokens=line.split()
#    x += [float(tokens[0][1:])]
    nextX = float(tokens[0][1:])
    if nextX == 0 and xIsLogScale:
      x += [0.5]
    else:
      x += [nextX]

    i = 1
    while i<len(tokens):
      if len(y)<=i//2:
        y += [[]]
        l += [measurementName+':'+tokens[i]]
      y[i//2] += [float(tokens[i+1])]
#      nextY = float(tokens[i+1])
#      if nextY == 0 and yIsLogScale:
#        y[i//2] += [0.5]
#      else:
#        y[i//2] += [nextY]
      i+=2
i = 0
while i < len(y):
  plt.plot(x, y[i], label=l[i], marker=markers[nextMarker], color=colors[nextColor])
  nextMarker = (nextMarker+1)%len(markers)
  i+=1

if yIsLogScale:
  plt.yscale('log')
  ylocations=[0.3, 0.5, 1, 2, 4, 8, 16, 32, 38]
  ax.yaxis.set_minor_locator(ticker.FixedLocator(locs=ylocations)) #set the ticks position
  ax.yaxis.set_major_formatter(ticker.NullFormatter())   # remove the major ticks
  ax.yaxis.set_minor_formatter(ticker.FuncFormatter(ticks_format))  #add the custom ticks

if xIsLogScale:
  plt.xscale('log')
  ax.xaxis.set_minor_locator(ticker.FixedLocator(locs=x[1:])) #set the ticks position
  ax.xaxis.set_major_formatter(ticker.NullFormatter())   # remove the major ticks
  ax.xaxis.set_minor_formatter(ticker.FuncFormatter(ticks_format))  #add the custom ticks

plt.legend()
#plt.show()
plt.savefig(testname+'.pdf')

