#!/bin/bash

# ./toPdf.sh pathToDirWithLogFiles

PLOTTERDIR=$(pwd)
LOGSDIR=$1

cd ${LOGSDIR}

for log in `find -name *.log`
do
  python3 ${PLOTTERDIR}/plot.py ${log}
done
