#!/bin/bash

filename=$1
filename="${filename%.*}"
pdfcrop --margins '5 5 5 5' $1 ${filename}_cropped.pdf
