/*
 */
#include <linux/module.h>	/* Needed by all modules */
#include <linux/kernel.h>	/* Needed for KERN_INFO */
#include <linux/random.h>
#include <linux/smp.h>
#include <linux/interrupt.h>
#include <linux/irqchip/arm-gic.h>
#include <linux/sched.h>   //wake_up_process()
#include <linux/kthread.h> //kthread_create(), kthread_run()
#include <linux/delay.h> //sleep
#include <linux/platform/tegra/mc.h>
/*#include <linux/platform/tegra/mc-regs-t18x.h> // included from linux/platform/tegra/mc.h*/
#include <linux/platform/tegra/mcerr_ecc_t18x.h>
#include <linux/platform/tegra/tegra18_emc.h>
#include <asm/io.h>
#include <linux/time.h>
#include <linux/highmem.h>
#include <linux/vmalloc.h>

#define DRVR_NAME "read_tegra_regs"

#define PTSA_REGS 0
#define PTSA_REGS_NAME "PTSA (DDA) RELATED REGISTERS"

#define THROTTLE_REGS 1
#define THROTTLE_REGS_NAME "THROTTLE RELATED REGISTERS"
static u32 ring0_throttle_mask   ;
static u32 niso_throttle_mask    ;
static u32 niso_throttle_mask_1  ;
static u32 ring1_throttle        ;
static u32 ring3_throttle        ;
static u32 niso_throttle         ;
static u32 outstanding_req       ;
static u32 outstanding_req_ring3 ;
static u32 outstanding_req_niso  ;


#if !defined(__aarch64__)
	#error Module can only be compiled on ARM 64 machines.
#endif

static int regsGroup = 0;





module_param(regsGroup, int,0);

void readRegs(void);
void readRegs(){
  switch(regsGroup) {
    case PTSA_REGS:
      printk(KERN_INFO
      "\n### PTSA / DDA related registers ###\n");
      printk(KERN_INFO
      "MC_SCEPC_PTSA_MIN        0x%x \n", mc_readl(MC_SCEPC_PTSA_MIN));
      printk(KERN_INFO
      "MC_AUD_PTSA_MIN          0x%x \n", mc_readl(MC_AUD_PTSA_MIN));
      printk(KERN_INFO
      "MC_EQOSPC_PTSA_MAX       0x%x \n", mc_readl(MC_EQOSPC_PTSA_MAX));
      printk(KERN_INFO
      "MC_APEDMAPC_PTSA_RATE    0x%x \n", mc_readl(MC_APEDMAPC_PTSA_RATE));
      printk(KERN_INFO
      "MC_MLL_MPCORER_PTSA_RATE 0x%x \n", mc_readl(MC_MLL_MPCORER_PTSA_RATE));
      printk(KERN_INFO
      "MC_RING2_PTSA_RATE       0x%x \n", mc_readl(MC_RING2_PTSA_RATE));
      printk(KERN_INFO
      "MC_USBD_PTSA_RATE        0x%x \n", mc_readl(MC_USBD_PTSA_RATE));
      printk(KERN_INFO
      "MC_USBX_PTSA_MIN         0x%x \n", mc_readl(MC_USBX_PTSA_MIN));
      printk(KERN_INFO
      "MC_JPG_PTSA_RATE         0x%x \n", mc_readl(MC_JPG_PTSA_RATE));
      printk(KERN_INFO
      "MC_APB_PTSA_MAX          0x%x \n", mc_readl(MC_APB_PTSA_MAX));
      printk(KERN_INFO
      "MC_USBD_PTSA_MIN         0x%x \n", mc_readl(MC_USBD_PTSA_MIN));
      printk(KERN_INFO
      "MC_DIS_PTSA_MIN          0x%x \n", mc_readl(MC_DIS_PTSA_MIN));
      printk(KERN_INFO
      "MC_RING1_PTSA_MIN        0x%x \n", mc_readl(MC_RING1_PTSA_MIN));
      printk(KERN_INFO
      "MC_DIS_PTSA_MAX          0x%x \n", mc_readl(MC_DIS_PTSA_MAX));
      printk(KERN_INFO
      "MC_SD_PTSA_MAX           0x%x \n", mc_readl(MC_SD_PTSA_MAX));
      printk(KERN_INFO
      "MC_MSE_PTSA_RATE         0x%x \n", mc_readl(MC_MSE_PTSA_RATE));
      printk(KERN_INFO
      "MC_PCX_PTSA_MAX          0x%x \n", mc_readl(MC_PCX_PTSA_MAX));
      printk(KERN_INFO
      "MC_VICPC_PTSA_MIN        0x%x \n", mc_readl(MC_VICPC_PTSA_MIN));
      printk(KERN_INFO
      "MC_AONDMAPC_PTSA_MIN     0x%x \n", mc_readl(MC_AONDMAPC_PTSA_MIN));
      printk(KERN_INFO
      "MC_ISP_PTSA_RATE         0x%x \n", mc_readl(MC_ISP_PTSA_RATE));
      printk(KERN_INFO
      "MC_RING2_PTSA_MAX        0x%x \n", mc_readl(MC_RING2_PTSA_MAX));
      printk(KERN_INFO
      "MC_BPMPDMAPC_PTSA_MIN    0x%x \n", mc_readl(MC_BPMPDMAPC_PTSA_MIN));
      printk(KERN_INFO
      "MC_AUD_PTSA_RATE         0x%x \n", mc_readl(MC_AUD_PTSA_RATE));
      printk(KERN_INFO
      "MC_HOST_PTSA_MIN         0x%x \n", mc_readl(MC_HOST_PTSA_MIN));
      printk(KERN_INFO
      "MC_SDM1_PTSA_RATE        0x%x \n", mc_readl(MC_SDM1_PTSA_RATE));
      printk(KERN_INFO
      "MC_MLL_MPCORER_PTSA_MAX  0x%x \n", mc_readl(MC_MLL_MPCORER_PTSA_MAX));
      printk(KERN_INFO
      "MC_SD_PTSA_MIN           0x%x \n", mc_readl(MC_SD_PTSA_MIN));
      printk(KERN_INFO
      "MC_NIC_PTSA_MAX          0x%x \n", mc_readl(MC_NIC_PTSA_MAX));
      printk(KERN_INFO
      "MC_NVD_PTSA_MAX          0x%x \n", mc_readl(MC_NVD_PTSA_MAX));
      printk(KERN_INFO
      "MC_RING1_PTSA_RATE       0x%x \n", mc_readl(MC_RING1_PTSA_RATE));
      printk(KERN_INFO
      "MC_JPG_PTSA_MIN          0x%x \n", mc_readl(MC_JPG_PTSA_MIN));
      printk(KERN_INFO
      "MC_AONPC_PTSA_MAX        0x%x \n", mc_readl(MC_AONPC_PTSA_MAX));
      printk(KERN_INFO
      "MC_MSE2_PTSA_MAX         0x%x \n", mc_readl(MC_MSE2_PTSA_MAX));
      printk(KERN_INFO
      "MC_HDAPC_PTSA_MIN        0x%x \n", mc_readl(MC_HDAPC_PTSA_MIN));
      printk(KERN_INFO
      "MC_JPG_PTSA_MAX          0x%x \n", mc_readl(MC_JPG_PTSA_MAX));
      printk(KERN_INFO
      "MC_VE_PTSA_MAX           0x%x \n", mc_readl(MC_VE_PTSA_MAX));
      printk(KERN_INFO
      "MC_UFSHCPC_PTSA_MAX      0x%x \n", mc_readl(MC_UFSHCPC_PTSA_MAX));
      printk(KERN_INFO
      "MC_DFD_PTSA_MAX          0x%x \n", mc_readl(MC_DFD_PTSA_MAX));
      printk(KERN_INFO
      "MC_SCEDMAPC_PTSA_RATE    0x%x \n", mc_readl(MC_SCEDMAPC_PTSA_RATE));
      printk(KERN_INFO
      "MC_VICPC_PTSA_RATE       0x%x \n", mc_readl(MC_VICPC_PTSA_RATE));
      printk(KERN_INFO
      "MC_BPMPPC_PTSA_RATE      0x%x \n", mc_readl(MC_BPMPPC_PTSA_RATE));
      printk(KERN_INFO
      "MC_GK_PTSA_MAX           0x%x \n", mc_readl(MC_GK_PTSA_MAX));
      printk(KERN_INFO
      "MC_SCEPC_PTSA_MAX        0x%x \n", mc_readl(MC_SCEPC_PTSA_MAX));
      printk(KERN_INFO
      "MC_SCEDMAPC_PTSA_MIN     0x%x \n", mc_readl(MC_SCEDMAPC_PTSA_MIN));
      printk(KERN_INFO
      "MC_VICPC_PTSA_MAX        0x%x \n", mc_readl(MC_VICPC_PTSA_MAX));
      printk(KERN_INFO
      "MC_SDM_PTSA_MAX          0x%x \n", mc_readl(MC_SDM_PTSA_MAX));
      printk(KERN_INFO
      "MC_NVD_PTSA_RATE         0x%x \n", mc_readl(MC_NVD_PTSA_RATE));
      printk(KERN_INFO
      "MC_MSE2_PTSA_MIN         0x%x \n", mc_readl(MC_MSE2_PTSA_MIN));
      printk(KERN_INFO
      "MC_PCX_PTSA_MIN          0x%x \n", mc_readl(MC_PCX_PTSA_MIN));
      printk(KERN_INFO
      "MC_SAX_PTSA_RATE         0x%x \n", mc_readl(MC_SAX_PTSA_RATE));
      printk(KERN_INFO
      "MC_APB_PTSA_MIN          0x%x \n", mc_readl(MC_APB_PTSA_MIN));
      printk(KERN_INFO
      "MC_SCEPC_PTSA_RATE       0x%x \n", mc_readl(MC_SCEPC_PTSA_RATE));
      printk(KERN_INFO
      "MC_EQOSPC_PTSA_MIN       0x%x \n", mc_readl(MC_EQOSPC_PTSA_MIN));
      printk(KERN_INFO
      "MC_EQOSPC_PTSA_RATE      0x%x \n", mc_readl(MC_EQOSPC_PTSA_RATE));
      printk(KERN_INFO
      "MC_AONDMAPC_PTSA_MAX     0x%x \n", mc_readl(MC_AONDMAPC_PTSA_MAX));
      printk(KERN_INFO
      "MC_AONPC_PTSA_MIN        0x%x \n", mc_readl(MC_AONPC_PTSA_MIN));
      printk(KERN_INFO
      "MC_VICPC3_PTSA_MIN       0x%x \n", mc_readl(MC_VICPC3_PTSA_MIN));
      printk(KERN_INFO
      "MC_GK2_PTSA_MIN          0x%x \n", mc_readl(MC_GK2_PTSA_MIN));
      printk(KERN_INFO
      "MC_PCX_PTSA_RATE         0x%x \n", mc_readl(MC_PCX_PTSA_RATE));
      printk(KERN_INFO
      "MC_RING1_PTSA_MAX        0x%x \n", mc_readl(MC_RING1_PTSA_MAX));
      printk(KERN_INFO
      "MC_HDAPC_PTSA_RATE       0x%x \n", mc_readl(MC_HDAPC_PTSA_RATE));
      printk(KERN_INFO
      "MC_MLL_MPCORER_PTSA_MIN  0x%x \n", mc_readl(MC_MLL_MPCORER_PTSA_MIN));
      printk(KERN_INFO
      "MC_GK2_PTSA_MAX          0x%x \n", mc_readl(MC_GK2_PTSA_MAX));
      printk(KERN_INFO
      "MC_SDM1_PTSA_MIN         0x%x \n", mc_readl(MC_SDM1_PTSA_MIN));
      printk(KERN_INFO
      "MC_VICPC3_PTSA_RATE      0x%x \n", mc_readl(MC_VICPC3_PTSA_RATE));
      printk(KERN_INFO
      "MC_AUD_PTSA_MAX          0x%x \n", mc_readl(MC_AUD_PTSA_MAX));
      printk(KERN_INFO
      "MC_GK2_PTSA_RATE         0x%x \n", mc_readl(MC_GK2_PTSA_RATE));
      printk(KERN_INFO
      "MC_NVD3_PTSA_MAX         0x%x \n", mc_readl(MC_NVD3_PTSA_MAX));
      printk(KERN_INFO
      "MC_ISP_PTSA_MAX          0x%x \n", mc_readl(MC_ISP_PTSA_MAX));
      printk(KERN_INFO
      "MC_NVD_PTSA_MIN          0x%x \n", mc_readl(MC_NVD_PTSA_MIN));
      printk(KERN_INFO
      "MC_UFSHCPC_PTSA_MIN      0x%x \n", mc_readl(MC_UFSHCPC_PTSA_MIN));
      printk(KERN_INFO
      "MC_FTOP_PTSA_RATE        0x%x \n", mc_readl(MC_FTOP_PTSA_RATE));
      printk(KERN_INFO
      "MC_DFD_PTSA_MIN          0x%x \n", mc_readl(MC_DFD_PTSA_MIN));
      printk(KERN_INFO
      "MC_VICPC3_PTSA_MAX       0x%x \n", mc_readl(MC_VICPC3_PTSA_MAX));
      printk(KERN_INFO
      "MC_NVD3_PTSA_MIN         0x%x \n", mc_readl(MC_NVD3_PTSA_MIN));
      printk(KERN_INFO
      "MC_USBX_PTSA_MAX         0x%x \n", mc_readl(MC_USBX_PTSA_MAX));
      printk(KERN_INFO
      "MC_DIS_PTSA_RATE         0x%x \n", mc_readl(MC_DIS_PTSA_RATE));
      printk(KERN_INFO
      "MC_USBD_PTSA_MAX         0x%x \n", mc_readl(MC_USBD_PTSA_MAX));
      printk(KERN_INFO
      "MC_APEDMAPC_PTSA_MAX     0x%x \n", mc_readl(MC_APEDMAPC_PTSA_MAX));
      printk(KERN_INFO
      "MC_USBX_PTSA_RATE        0x%x \n", mc_readl(MC_USBX_PTSA_RATE));
      printk(KERN_INFO
      "MC_BPMPDMAPC_PTSA_MAX    0x%x \n", mc_readl(MC_BPMPDMAPC_PTSA_MAX));
      printk(KERN_INFO
      "MC_FTOP_PTSA_MAX         0x%x \n", mc_readl(MC_FTOP_PTSA_MAX));
      printk(KERN_INFO
      "MC_HDAPC_PTSA_MAX        0x%x \n", mc_readl(MC_HDAPC_PTSA_MAX));
      printk(KERN_INFO
      "MC_SD_PTSA_RATE          0x%x \n", mc_readl(MC_SD_PTSA_RATE));
      printk(KERN_INFO
      "MC_BPMPDMAPC_PTSA_RATE   0x%x \n", mc_readl(MC_BPMPDMAPC_PTSA_RATE));
      printk(KERN_INFO
      "MC_DFD_PTSA_RATE         0x%x \n", mc_readl(MC_DFD_PTSA_RATE));
      printk(KERN_INFO
      "MC_SDM_PTSA_RATE         0x%x \n", mc_readl(MC_SDM_PTSA_RATE));
      printk(KERN_INFO
      "MC_FTOP_PTSA_MIN         0x%x \n", mc_readl(MC_FTOP_PTSA_MIN));
      printk(KERN_INFO
      "MC_SDM_PTSA_MIN          0x%x \n", mc_readl(MC_SDM_PTSA_MIN));
      printk(KERN_INFO
      "MC_APB_PTSA_RATE         0x%x \n", mc_readl(MC_APB_PTSA_RATE));
      printk(KERN_INFO
      "MC_RING2_PTSA_MIN        0x%x \n", mc_readl(MC_RING2_PTSA_MIN));
      printk(KERN_INFO
      "MC_SMMU_SMMU_PTSA_MAX    0x%x \n", mc_readl(MC_SMMU_SMMU_PTSA_MAX));
      printk(KERN_INFO
      "MC_UFSHCPC_PTSA_RATE     0x%x \n", mc_readl(MC_UFSHCPC_PTSA_RATE));
      printk(KERN_INFO
      "MC_BPMPPC_PTSA_MAX       0x%x \n", mc_readl(MC_BPMPPC_PTSA_MAX));
      printk(KERN_INFO
      "MC_MSE2_PTSA_RATE        0x%x \n", mc_readl(MC_MSE2_PTSA_RATE));
      printk(KERN_INFO
      "MC_MSE_PTSA_MIN          0x%x \n", mc_readl(MC_MSE_PTSA_MIN));
      printk(KERN_INFO
      "MC_NVD3_PTSA_RATE        0x%x \n", mc_readl(MC_NVD3_PTSA_RATE));
      printk(KERN_INFO
      "MC_HOST_PTSA_RATE        0x%x \n", mc_readl(MC_HOST_PTSA_RATE));
      printk(KERN_INFO
      "MC_VE_PTSA_RATE          0x%x \n", mc_readl(MC_VE_PTSA_RATE));
      printk(KERN_INFO
      "MC_AONPC_PTSA_RATE       0x%x \n", mc_readl(MC_AONPC_PTSA_RATE));
      printk(KERN_INFO
      "MC_SAX_PTSA_MIN          0x%x \n", mc_readl(MC_SAX_PTSA_MIN));
      printk(KERN_INFO
      "MC_NIC_PTSA_RATE         0x%x \n", mc_readl(MC_NIC_PTSA_RATE));
      printk(KERN_INFO
      "MC_SMMU_SMMU_PTSA_MIN    0x%x \n", mc_readl(MC_SMMU_SMMU_PTSA_MIN));
      printk(KERN_INFO
      "MC_SCEDMAPC_PTSA_MAX     0x%x \n", mc_readl(MC_SCEDMAPC_PTSA_MAX));
      printk(KERN_INFO
      "MC_ISP_PTSA_MIN          0x%x \n", mc_readl(MC_ISP_PTSA_MIN));
      printk(KERN_INFO
      "MC_AONDMAPC_PTSA_RATE    0x%x \n", mc_readl(MC_AONDMAPC_PTSA_RATE));
      printk(KERN_INFO
      "MC_SDM1_PTSA_MAX         0x%x \n", mc_readl(MC_SDM1_PTSA_MAX));
      printk(KERN_INFO
      "MC_HOST_PTSA_MAX         0x%x \n", mc_readl(MC_HOST_PTSA_MAX));
      printk(KERN_INFO
      "MC_BPMPPC_PTSA_MIN       0x%x \n", mc_readl(MC_BPMPPC_PTSA_MIN));
      printk(KERN_INFO
      "MC_SAX_PTSA_MAX          0x%x \n", mc_readl(MC_SAX_PTSA_MAX));
      printk(KERN_INFO
      "MC_VE_PTSA_MIN           0x%x \n", mc_readl(MC_VE_PTSA_MIN));
      printk(KERN_INFO
      "MC_APEDMAPC_PTSA_MIN     0x%x \n", mc_readl(MC_APEDMAPC_PTSA_MIN));
      printk(KERN_INFO
      "MC_GK_PTSA_MIN           0x%x \n", mc_readl(MC_GK_PTSA_MIN));
      printk(KERN_INFO
      "MC_MSE_PTSA_MAX          0x%x \n", mc_readl(MC_MSE_PTSA_MAX));
      printk(KERN_INFO
      "MC_SMMU_SMMU_PTSA_RATE   0x%x \n", mc_readl(MC_SMMU_SMMU_PTSA_RATE));
      printk(KERN_INFO
      "MC_NIC_PTSA_MIN          0x%x \n", mc_readl(MC_NIC_PTSA_MIN));
      printk(KERN_INFO
      "MC_GK_PTSA_RATE          0x%x \n", mc_readl(MC_GK_PTSA_RATE));
      printk(KERN_INFO
      "MC_PTSA_GRANT_DECREMENT  0x%x \n", mc_readl(MC_PTSA_GRANT_DECREMENT));

      printk(KERN_INFO
      "MC_ROC_DMA_R_PTSA_MIN    0x%x \n", mc_readl(MC_ROC_DMA_R_PTSA_MIN));
      printk(KERN_INFO
      "MC_ROC_DMA_R_PTSA_MAX    0x%x \n", mc_readl(MC_ROC_DMA_R_PTSA_MAX));
      printk(KERN_INFO
      "MC_ROC_DMA_R_PTSA_RATE   0x%x \n", mc_readl(MC_ROC_DMA_R_PTSA_RATE));
      printk(KERN_INFO
      "MC_RING1_WR_B_PTSA_MIN   0x%x \n", mc_readl(MC_RING1_WR_B_PTSA_MIN));
      printk(KERN_INFO
      "MC_RING1_WR_B_PTSA_MAX   0x%x \n", mc_readl(MC_RING1_WR_B_PTSA_MAX));
      printk(KERN_INFO
      "MC_RING1_WR_B_PTSA_RATE  0x%x \n", mc_readl(MC_RING1_WR_B_PTSA_RATE));
      printk(KERN_INFO
      "MC_RING1_WR_NB_PTSA_MIN  0x%x \n", mc_readl(MC_RING1_WR_NB_PTSA_MIN));
      printk(KERN_INFO
      "MC_RING1_WR_NB_PTSA_MAX  0x%x \n", mc_readl(MC_RING1_WR_NB_PTSA_MAX));
      printk(KERN_INFO
      "MC_RING1_WR_NB_PTSA_RATE 0x%x \n", mc_readl(MC_RING1_WR_NB_PTSA_RATE));
      printk(KERN_INFO
      "MC_RING1_RD_B_PTSA_MIN   0x%x \n", mc_readl(MC_RING1_RD_B_PTSA_MIN));
      printk(KERN_INFO
      "MC_RING1_RD_B_PTSA_MAX   0x%x \n", mc_readl(MC_RING1_RD_B_PTSA_MAX));
      printk(KERN_INFO
      "MC_RING1_RD_B_PTSA_RATE  0x%x \n", mc_readl(MC_RING1_RD_B_PTSA_RATE));
      printk(KERN_INFO
      "MC_RING1_RD_NB_PTSA_MIN  0x%x \n", mc_readl(MC_RING1_RD_NB_PTSA_MIN));
      printk(KERN_INFO
      "MC_RING1_RD_NB_PTSA_MAX  0x%x \n", mc_readl(MC_RING1_RD_NB_PTSA_MAX));
      printk(KERN_INFO
      "MC_RING1_RD_NB_PTSA_RATE 0x%x \n", mc_readl(MC_RING1_RD_NB_PTSA_RATE));

      printk(KERN_INFO "\n### latency ###\n");
      break;
    case THROTTLE_REGS:
      ring0_throttle_mask   =mc_readl(MC_EMEM_ARB_RING0_THROTTLE_MASK)  ;
      niso_throttle_mask    =mc_readl(MC_EMEM_ARB_NISO_THROTTLE_MASK)   ;
      niso_throttle_mask_1  =mc_readl(MC_EMEM_ARB_NISO_THROTTLE_MASK_1) ;
      ring1_throttle        =mc_readl(MC_EMEM_ARB_RING1_THROTTLE)       ;
      ring3_throttle        =mc_readl(MC_EMEM_ARB_RING3_THROTTLE)       ;
      niso_throttle         =mc_readl(MC_EMEM_ARB_NISO_THROTTLE)        ;
      outstanding_req       =mc_readl(MC_EMEM_ARB_OUTSTANDING_REQ)      ;
      outstanding_req_ring3 =mc_readl(MC_EMEM_ARB_OUTSTANDING_REQ_RING3);
      outstanding_req_niso  =mc_readl(MC_EMEM_ARB_OUTSTANDING_REQ_NISO) ;

      printk(KERN_INFO "### RING0 throttle ###\n");
      printk(KERN_INFO "MC_EMEM_ARB_RING0_THROTTLE_MASK    0x%x\n",                                                     ring0_throttle_mask);
      printk(KERN_INFO "MC_EMEM_ARB_RING1_THROTTLE         0x%x\tth_cyc_high: %d\tth_cyc_low: %d\n",                    ring1_throttle, (ring1_throttle & 0x003f0000) >> 16, ring1_throttle & 0x3f);
      printk(KERN_INFO "MC_EMEM_ARB_OUTSTANDING_REQ        0x%x\tcount: %d\tmax: %d\tlim_en: %d lim_holdoff: %d\n",  outstanding_req, (outstanding_req & 0x03ff0000) >> 16, outstanding_req & 0x3ff, outstanding_req >> 31, (outstanding_req >> 30) & 1);
      printk(KERN_INFO "### RING2 throttle ###\n");
      printk(KERN_INFO "MC_EMEM_ARB_RING3_THROTTLE         0x%x\tth_cyc_high: %d\tth_cyc: %d\n",                        ring3_throttle, (ring3_throttle & 0x001f0000) >> 16, ring3_throttle & 0x1f);
      printk(KERN_INFO "MC_EMEM_ARB_OUTSTANDING_REQ_RING3  0x%x\tcount: %d\tmax: %d\tlim_en: %d lim_holdoff: %d\n",  outstanding_req_ring3, (outstanding_req_ring3 & 0x03ff0000) >> 16, outstanding_req_ring3 & 0x3ff, outstanding_req_ring3 >> 31, (outstanding_req_ring3 >> 30) & 1);
      printk(KERN_INFO "### RING2-NISO throttle ###\n");
      printk(KERN_INFO "MC_EMEM_ARB_NISO_THROTTLE_MASK     0x%x\n",                                                     niso_throttle_mask);
      printk(KERN_INFO "MC_EMEM_ARB_NISO_THROTTLE_MASK_1   0x%x\n",                                                     niso_throttle_mask_1);
      printk(KERN_INFO "MC_EMEM_ARB_NISO_THROTTLE          0x%x\tth_cyc_high: %d\tth_cyc: %d\tdisable_cnt_event: %d\n", niso_throttle, (niso_throttle & 0x003f0000) >> 16, niso_throttle & 0x3f, niso_throttle >> 31);
      printk(KERN_INFO "MC_EMEM_ARB_OUTSTANDING_REQ_NISO   0x%x\t         \tmax: %d\tlim_en: %d lim_holdoff: %d\n",  outstanding_req_niso, outstanding_req_niso & 0x3ff, outstanding_req_niso >> 31, (outstanding_req_niso >> 30) & 1);
//      printk(KERN_INFO "  0x%x\n", mc_readl());

      break;
  }
}

int init_module(void)
{
  char *name;
  switch(regsGroup) {
    case PTSA_REGS:
      name = PTSA_REGS_NAME;
      break;
    case THROTTLE_REGS:
      name = THROTTLE_REGS_NAME;
      break;
  }
  printk(KERN_INFO "start readregs.ko %d : %s\n", regsGroup, name);
  readRegs();

	return 0;
}


void cleanup_module(void)
{
  //printk(KERN_INFO "konecny read 0x%x \n",mc_readl(MC_EMEM_ARB_RING0_THROTTLE_MASK));
  //printk(KERN_INFO "ring 1 throttle 0x%x \n",mc_readl(MC_EMEM_ARB_OUTSTANDING_REQ_RING3));
  //printk(KERN_INFO "ring 1 throttle 0x%x \n",mc_readl(MC_EMEM_ARB_OUTSTANDING_REQ_NISO));
}

MODULE_LICENSE("GPL");
