# call with: PATH="$PATH:/opt/OSELAS.Toolchain-2014.12.2/aarch64-v8a-linux-gnu/gcc-4.9.2-glibc-2.20-binutils-2.24-kernel-3.16-sanitized/bin/" make

obj-m += throttleOrig/throttle.o
obj-m += setThrottleRegsTool/setThrottleRegs.o
obj-m += readSetsOfRegsTool/readregs.o
#KDIR=../../../jailhouse-build/build/kernel-4.4/
KDIR=../../../public_release/kernel/kernel-4.4/

cross:
	PATH="${PATH}:/opt/OSELAS.Toolchain-2014.12.2/aarch64-v8a-linux-gnu/gcc-4.9.2-glibc-2.20-binutils-2.24-kernel-3.16-sanitized/bin/" make -C $(KDIR) M=$(PWD) modules ARCH=arm64 CROSS_COMPILE=aarch64-v8a-linux-gnu-

all: 
	make -C $(KDIR) M=$(PWD) modules ARCH=arm64 CROSS_COMPILE=aarch64-v8a-linux-gnu-

clean:
	make -C $(KDIR) M=$(PWD) clean
